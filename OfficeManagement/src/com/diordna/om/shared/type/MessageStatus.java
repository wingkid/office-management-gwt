package com.diordna.om.shared.type;

public enum MessageStatus {
	
	STATUS_OK(19800200),
	
	UNKNOWN(-19780000),
	;
	
	private int code = -1;
	
	MessageStatus(int code) {
		this.code = code;
	}
	
	public int value() {
		return this.code;
	}
	
	public static MessageStatus valueOf(int code) {
		if (MessageStatus.STATUS_OK.value() == code) {
			return MessageStatus.STATUS_OK;
		}else if (MessageStatus.STATUS_OK.value() == code) {
			return MessageStatus.STATUS_OK;
		}
		return MessageStatus.UNKNOWN;
	}
	
}