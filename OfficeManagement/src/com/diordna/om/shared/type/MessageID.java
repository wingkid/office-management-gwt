package com.diordna.om.shared.type;

public enum MessageID {
	
	MEETING_INITIAL(19800100),
	
	MEETING_CREATION(19800100),
	
	MEETING_LIST(19800102),
	
	UNKNOWN(-19800000),
	;
	
	private int code = -1;
	
	MessageID(int code) {
		this.code = code;
	}
	
	public int value() {
		return this.code;
	}
	
	public static MessageID valueOf(int code) {
		if (MessageID.MEETING_LIST.value() == code) {
			return MessageID.MEETING_LIST;
		}else if (MessageID.MEETING_LIST.value() == code) {
			return MessageID.MEETING_LIST;
		}
		return MessageID.UNKNOWN;
	}
	
}