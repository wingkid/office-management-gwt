package com.diordna.om.shared.type;

/**
 * @author Rui Wang
 */
public enum HttpString {
	
	SEPARATOR("/"),
	APPLICATION("officemanagement"),
	CONTENT_TYPE("Content-Type"),
	CONTENT_TYPE_JSON("application/json"),
	REST_MEETING("meeting"),
	REST_MEETING_INITIAL("initial"),
	ENCODING("UTF-8"),
	;
	
	private String code = null;
	
	HttpString(String code) {
		this.code = code;
	}
	
	public String value() {
		return this.code;
	}
	
	@Override
	public String toString() {
		return this.code;
	}
	

}