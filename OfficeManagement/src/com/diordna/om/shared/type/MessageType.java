package com.diordna.om.shared.type;

public enum MessageType {
	
	MSG_ID("MID"),
	MSG_SD("MSD");
	
	private String code = null;
	
	MessageType(String code) {
		this.code = code;
	}
	
	public String value() {
		return this.code;
	}
	
}