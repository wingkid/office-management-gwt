/**
 * 
 */
package com.diordna.om.shared.data;

import com.diordna.om.shared.type.MessageID;
import com.diordna.om.shared.type.MessageStatus;

/**
 * @author Rui
 *
 */
public abstract class Message {
	
	public final static String HEADER_KEY = "MSGHDR";
	protected final static String CONTENT_KEY = "MSGCNT";
	protected MessageHeader header = null;
	
	protected void setHeader(MessageID id, MessageStatus status, boolean client) {
		this.header = new MessageHeader();
		this.header.setId(id);
		this.header.setStatus(status);
		this.header.setClient(client);
	}
	
	protected void setHeader(int id, int status, boolean client) {
		this.header = new MessageHeader();
		this.header.setId(MessageID.valueOf(id));
		this.header.setStatus(MessageStatus.valueOf(status));
		this.header.setClient(client);
	}
	
	protected void setHeader(MessageID id, boolean client) {
		this.header = new MessageHeader();
		this.header.setId(id);
		this.header.setClient(client);
	}
	
	protected void setHeader(int id, boolean client) {
		this.header = new MessageHeader();
		this.header.setId(MessageID.valueOf(id));
		this.header.setClient(client);
	}
	
	protected abstract String getMessageJson();
	
}