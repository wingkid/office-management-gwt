package com.diordna.om.shared.data.meeting;

import com.diordna.om.shared.data.Message;
import com.diordna.om.shared.type.MessageID;

public abstract class ResponseMeetingInitial extends Message {
	
	protected enum KEY {
		
		OFFICE, OFFSET, ROOM, ROOM_ID, ROOM_NAME, ROOM_AVAILABLE, STAFF, STAFF_ID, STAFF_NAME;
		
	}
	
	public ResponseMeetingInitial() {
		this.setHeader(MessageID.MEETING_INITIAL, false);
	}

}