package com.diordna.om.shared.data.meeting;

import com.diordna.om.shared.data.Message;
import com.diordna.om.shared.type.MessageID;

public abstract class RequestMeetingList extends Message {
	
	protected enum KEY {
		
		Y, M, D, O;
		
	}
	
	protected int date[] = null;
	
	protected RequestMeetingList() {
		this.setHeader(MessageID.MEETING_LIST, true);
	}
	
	protected void setData(int date[]) {
		this.date = date;
	}
	
}