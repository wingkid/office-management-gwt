package com.diordna.om.shared.data.meeting;

import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.shared.data.Message;
import com.diordna.om.shared.type.MessageID;

public abstract class RequestMeetingCreation extends Message {
	
	protected enum KEY {
		ID, ROOM_ID, CREATED_BY, CREATE_TS, UPDATED_BY, UPDATE_TS, DESCRIPTION, ALL_DAY, START_TIME, END_TIME, BOOKING_DATE, REPEAT, EXPIRE_DATE
		
		;
		
	}
	
	protected Meeting meeting = null;
	
	public RequestMeetingCreation() {
		this.setHeader(MessageID.MEETING_CREATION, true);
	}
	
	protected void setData(Meeting meeting) {
		this.meeting = meeting;
	}
	
	public Meeting getData() {
		return this.meeting;
	}
	
}