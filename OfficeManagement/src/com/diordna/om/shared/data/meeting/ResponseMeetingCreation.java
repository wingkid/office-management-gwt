package com.diordna.om.shared.data.meeting;

import com.diordna.om.shared.data.Message;
import com.diordna.om.shared.type.MessageID;

public abstract class ResponseMeetingCreation extends Message {
	
	public ResponseMeetingCreation() {
		this.setHeader(MessageID.MEETING_CREATION, false);
	}
	
}