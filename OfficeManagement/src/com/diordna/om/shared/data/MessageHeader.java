package com.diordna.om.shared.data;

import com.diordna.om.shared.type.MessageID;
import com.diordna.om.shared.type.MessageStatus;

public class MessageHeader {
	
	public enum KEY {
		
		MSG_ID, MSG_STATUS, MSG_CLIENT;
		
	}
	
	private boolean client = true;
	private MessageID id = MessageID.UNKNOWN;
	private MessageStatus status = MessageStatus.UNKNOWN;
	
	public MessageID getId() {
		return this.id;
	}
	
	public void setId(MessageID id) {
		this.id = id;
	}
	
	public boolean isClient() {
		return this.client;
	}
	
	public void setClient(boolean client) {
		this.client = client;
	}

	public MessageStatus getStatus() {
		return this.status;
	}

	public void setStatus(MessageStatus status) {
		this.status = status;
	}
	
	
	
}