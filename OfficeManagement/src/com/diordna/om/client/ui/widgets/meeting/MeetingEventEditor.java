package com.diordna.om.client.ui.widgets.meeting;

import java.util.Date;
import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.dataset.Staff;
import com.diordna.om.client.ui.resources.ButtonResources;
import com.diordna.om.client.ui.resources.MeetingResources;
import com.diordna.om.client.ui.util.UiTools;
import com.diordna.om.client.util.LanguageBag;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Label;
import com.diordna.om.client.ui.components.input.LabelledListBox;
import com.diordna.om.client.ui.components.input.LabelledTextArea;
import com.diordna.om.client.ui.components.input.LabelledLabel;
import com.diordna.om.client.ui.components.input.LabelledDateBox;

/**
 * @author Rui Wang
 */
public class MeetingEventEditor extends Composite implements ClickHandler, LabelledListBox.Handler {
	
	public interface Handler {
		
		public void onCreate(MeetingEventEditor source);
		
	}

	interface MeetingEventEditorUiBinder extends UiBinder<Widget, MeetingEventEditor> {}
	
	private static MeetingEventEditorUiBinder uiBinder = GWT.create(MeetingEventEditorUiBinder.class);
	@UiField SimplePanel frame;
	@UiField LayoutPanel headerBase;
	@UiField Label headerLabel;
	@UiField LayoutPanel contentBase;
	@UiField LayoutPanel actionBase;
	@UiField Button okButton;
	@UiField Button cancelButton;
	@UiField LabelledLabel bookedTime;
	@UiField LabelledListBox bookedByListBox;
	@UiField LabelledTextArea descriptionTextarea;
	@UiField LabelledListBox repeatListBox;
	@UiField LabelledDateBox expireBox;

	private DecoratedPopupPanel popupBase = new DecoratedPopupPanel(true);
	private Room[] rooms = null;
	private int room = -1;
//	private int from = -1;
//	private int to = -1;
	private int[] duration = null;
	private Date date = null;
	private Handler handler = null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public MeetingEventEditor() {
		MeetingResources.I.meetingCss().ensureInjected();
		ButtonResources.I.buttonCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		
		this.popupBase.setStyleName(null);
		this.popupBase.setAnimationEnabled(true);
		this.popupBase.setWidget(this);
		
		this.bookedTime.setLabelText("Booking time between");
		
		this.bookedByListBox.setLabelText("Booking By");
		this.bookedByListBox.addItem("Select...", "-1");
		
		this.descriptionTextarea.setLabelText("Description");
		this.descriptionTextarea.setTextAreaHeightInPX(90);
		
		this.repeatListBox.setLabelText("Repeat Option");
		this.repeatListBox.setHandler(this);
		this.repeatListBox.addItem("No repeat", "0");
		this.repeatListBox.addItem("Daily", "1");
		this.repeatListBox.addItem("Weekly", "2");
		
		this.expireBox.setLabelText("Repeat Expired Date");
		this.expireBox.setVisible(false);
		
		this.okButton.setText("Ok");
		this.okButton.setVisible(false);
		this.okButton.setStyleName(ButtonResources.I.buttonCss().whiteButton());
		this.okButton.addClickHandler(this);
		this.cancelButton.setText("Cancel");
		this.cancelButton.setStyleName(ButtonResources.I.buttonCss().whiteButton());
		this.cancelButton.addClickHandler(this);
	}
	
	private void reset() {
		this.bookedTime.setValueText("");
		this.bookedByListBox.setSelectedIndex(0);
		this.okButton.setVisible(false);
		this.descriptionTextarea.setTextAreaText("");
		this.repeatListBox.setSelectedIndex(0);
		this.expireBox.setVisible(false);
		this.expireBox.setDateBox(null);
	}
	
	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public void show(int room, int from, int to, int[] duration, Date date) {
		this.reset();
//		this.from = from;
//		this.to = to;
		this.duration = duration;
		this.date = date;
		this.bookedTime.setValueText(UiTools.getBetweenTime(duration[0], duration[1]));
		if (0 == room) {
			this.headerBase.setStyleName(MeetingResources.I.meetingCss().eventEditorHeaderRed());
			this.okButton.setStyleName(ButtonResources.I.buttonCss().redButton());
			this.cancelButton.setStyleName(ButtonResources.I.buttonCss().redButton());
		}else if (1 == room) {
			this.headerBase.setStyleName(MeetingResources.I.meetingCss().eventEditorHeaderOrange());
			this.okButton.setStyleName(ButtonResources.I.buttonCss().orangeButton());
			this.cancelButton.setStyleName(ButtonResources.I.buttonCss().orangeButton());
		}else if (2 == room) {
			this.headerBase.setStyleName(MeetingResources.I.meetingCss().eventEditorHeaderGreen());
			this.okButton.setStyleName(ButtonResources.I.buttonCss().greenButton());
			this.cancelButton.setStyleName(ButtonResources.I.buttonCss().greenButton());
		}else if (3 == room) {
			this.headerBase.setStyleName(MeetingResources.I.meetingCss().eventEditorHeaderBlue());
			this.okButton.setStyleName(ButtonResources.I.buttonCss().blueButton());
			this.cancelButton.setStyleName(ButtonResources.I.buttonCss().blueButton());
		}else{
			this.headerBase.setStyleName(MeetingResources.I.meetingCss().eventEditorHeaderRed());
			this.okButton.setStyleName(ButtonResources.I.buttonCss().whiteButton());
			this.cancelButton.setStyleName(ButtonResources.I.buttonCss().whiteButton());
		}
		this.headerLabel.setText(LanguageBag.meetingEventEditor$headerLabel$CreateEvent());
		if (!this.popupBase.isShowing()) {
			int w = Window.getClientWidth();
			int h = Window.getClientHeight();
			this.popupBase.setSize("620px", "450px");
			this.popupBase.setPopupPosition((w-620)/2, (h-450)/2);
			this.popupBase.show();
		}
	}
	
	public void initialRoom(Room[] rooms) {
		this.rooms = rooms;
	}

	public void initialStaff(Staff[] staffs) {
		if (null != staffs && staffs.length > 0) {
			for (Staff staff : staffs) {
				this.bookedByListBox.addItem(staff.getName(), ""+staff.getId());
			}
		}
		this.bookedByListBox.setHandler(this);
	}
	
	public int getRoomId() {
		if (null == this.rooms || this.room<0 || this.rooms.length<this.room) {
			return -1;
		}
		return this.rooms[this.room].getId();
	}

	public long getBookingDate() {
		return this.date.getTime();
	}

	public int getStartTime() {
		return this.duration[0];
	}

	public int getEndTime() {
		return this.duration[1];
	}

	@SuppressWarnings("deprecation")
	public int getRepeat() {
		int index = this.repeatListBox.getSelectedIndex();
		if (index == 0) {
			return -1;
		}else if (index == 9) {
			return 0;
		}else{
			return this.date.getDay();
		}
	}

	public long getExpriedDate() {
		if (this.repeatListBox.getSelectedIndex() > 0) {
			return this.expireBox.getDate().getTime();
		}else{
			return -1;
		}
	}

	public int getCreatedBy() {
		return Integer.parseInt(this.bookedByListBox.getSelectedValue());
	}

	public String getDescription() {
		return this.descriptionTextarea.getTextAreaText();
	}

	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == this.okButton) {
			if (null != this.handler && this.bookedByListBox.getSelectedIndex() > 0) {
				this.handler.onCreate(this);
			}
			this.popupBase.hide();
		}else if (source == this.cancelButton) {
			this.popupBase.hide();
		}
	}

	@Override
	public void onChangeHandler(LabelledListBox source, int index) {
		if (source.equals(this.bookedByListBox)) {
			if (index > 0) {
				this.okButton.setVisible(true);
			}else{
				this.okButton.setVisible(false);
			}
		}else if (source.equals(this.repeatListBox)) {
			if (index == 0) {
				this.expireBox.setVisible(false);
			}else{
				this.expireBox.setVisible(true);
			}
		}
	}

}