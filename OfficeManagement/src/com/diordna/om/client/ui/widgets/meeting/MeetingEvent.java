package com.diordna.om.client.ui.widgets.meeting;

import com.diordna.om.client.ui.resources.MeetingResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MeetingEvent extends Composite {
	
	public enum COLOR {
		
		RED, ORANGE, GREEN, BLUE;
		
	}

	interface MeetingEventUiBinder extends UiBinder<Widget, MeetingEvent> {}
	
	private static MeetingEventUiBinder uiBinder = GWT.create(MeetingEventUiBinder.class);
	@UiField LayoutPanel base;
	@UiField Label contentLabel;
	@UiField VerticalPanel titleLayout;
	@UiField Label titleLabel;
	@UiField VerticalPanel contentLayout;
	private int room = -1;
	private int from = -1;
	private int to = -1;
	private int copy = -1;
	private String id = null;
	private COLOR color = COLOR.RED;

	public MeetingEvent(String id, String title) {
		this.initWidget(uiBinder.createAndBindUi(this));
		this.id = id;
		this.titleLabel.setText(title);
		this.titleLabel.setStyleName(MeetingResources.I.meetingCss().eventHeaderText());
	}
	
	public void setColor(COLOR color) {
		this.color  = color;
		if (COLOR.RED.equals(color)) {
			this.base.setStyleName(MeetingResources.I.meetingCss().eventRed());
			this.titleLayout.setStyleName(MeetingResources.I.meetingCss().eventHeaderRed());
		}else if (COLOR.ORANGE.equals(color)) {
			this.base.setStyleName(MeetingResources.I.meetingCss().eventOrange());
			this.titleLayout.setStyleName(MeetingResources.I.meetingCss().eventHeaderOrange());
		}else if (COLOR.GREEN.equals(color)) {
			this.base.setStyleName(MeetingResources.I.meetingCss().eventGreen());
			this.titleLayout.setStyleName(MeetingResources.I.meetingCss().eventHeaderGreen());
		}else if (COLOR.BLUE.equals(color)) {
			this.base.setStyleName(MeetingResources.I.meetingCss().eventBlue());
			this.titleLayout.setStyleName(MeetingResources.I.meetingCss().eventHeaderBlue());
		}
	}
	
	public void setEvent(int room, int fromY, int toY) {
		this.room = room;
		this.from = fromY;
		this.to = toY;
	}
	
	public boolean isMyEvent(int room, int y) {
		return this.room == room && y > this.from && y <this.to;
	}

	public int getRoom() {
		return this.room;
	}

	public int getFrom() {
		return this.from;
	}

	public int getTo() {
		return this.to;
	}
	
	public int getCopy() {
		return this.copy;
	}

	public MeetingEvent copy(int copy) {
		MeetingEvent me = new MeetingEvent(this.id, this.titleLabel.getText());
		me.setColor(this.color);
		me.setEvent(this.room, this.from, this.to);
		me.copy = copy;
		return me;
	}

	public void setRoom(int room) {
		this.room = room;
	}
	
	public void setFrom(int from) {
		this.from = from;
	}
	
	public void setTo(int to) {
		this.to = to;
	}

	public COLOR getColor() {
		return this.color;
	}

	public String getId() {
		return this.id;
	}

}