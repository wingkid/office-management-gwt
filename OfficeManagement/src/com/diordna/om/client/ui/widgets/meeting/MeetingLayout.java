package com.diordna.om.client.ui.widgets.meeting;

import java.util.Date;

import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.dataset.Staff;
import com.diordna.om.client.ui.components.VerticalScrollPanel;
import com.diordna.om.client.ui.resources.MeetingResources;
import com.diordna.om.client.ui.util.TimeZone;
import com.diordna.om.client.ui.util.UiTools;
import com.diordna.om.client.ui.view.MeetingView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Button;
import com.diordna.om.client.ui.components.calendar.CalendarDialog;
import com.diordna.om.client.ui.components.calendar.CalendarMap;

/**
 * @author Rui Wang
 */
public class MeetingLayout extends Composite implements ClickHandler, CalendarDialog.Handler, MeetingConent.Handler, MeetingEventEditor.Handler, MeetingView {

	interface MeetingLayoutUiBinder extends UiBinder<Widget, MeetingLayout> {}
	
	private static MeetingLayoutUiBinder uiBinder = GWT.create(MeetingLayoutUiBinder.class);
	@UiField LayoutPanel base;
	@UiField LayoutPanel headerBase;
	@UiField VerticalScrollPanel contentBase;
	@UiField Label rm1Label;
	@UiField Label rm2Label;
	@UiField Label rm3Label;
	@UiField Label rm4Label;
	@UiField Image nationIcon;
	@UiField Label leftLabel;
	@UiField Label rightLabel;
	@UiField Label dateLabel;
	@UiField Label todayLabel;
	@UiField Button leftBut;
	@UiField Button dateBut;
	@UiField Button rightBut;
	@UiField Button todayBut;
	
	private Presenter presenter = null;
	private MeetingConent content = new MeetingConent(this);
	private Date date = UiTools.newDate();
	private CalendarDialog calendarDialog = null;

	@SuppressWarnings("deprecation")
	public MeetingLayout() {
		MeetingResources.I.meetingCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		this.contentBase.setConent(this.content);
		this.contentBase.refresh();
		this.nationIcon.setResource(MeetingResources.I.flagBlank());
		this.calendarDialog = new CalendarDialog();
		this.calendarDialog.setHandler(this);
		this.dateBut.addClickHandler(this);
		this.leftBut.addClickHandler(this);
		this.rightBut.addClickHandler(this);
		this.todayBut.addClickHandler(this);
		this.dateLabel.setText(this.date.getDate() + " " + CalendarMap.getCalendarMonth(new Date(this.date.getTime())));
	}
	
	@SuppressWarnings("deprecation")
	private int isToday() {
		Date today = new Date();
		int todayYear = 1900+today.getYear();
		int todayMonth = today.getMonth()+1;
		int todayDate = today.getDate();
		int todayInt = todayYear*10000+todayMonth*100+todayDate;
		int year = 1900+this.date.getYear();
		int month = this.date.getMonth()+1;
		int date = this.date.getDate();
		int dateInt = year*10000+month*100+date;
		return dateInt - todayInt;
	}
	
	@SuppressWarnings("deprecation")
	private int[] getDate() {
		return new int[]{this.date.getYear(), this.date.getMonth(), this.date.getDate(), this.date.getTimezoneOffset()};
	}
	
	private void fireRequestMeetingSetup() {
		if (null != this.presenter) {
			this.presenter.onRequestMeetingInitial();
		}
	}
	
	private void requestEventList() {
		if (null != this.presenter) {
			int date[] = this.getDate();
			this.presenter.onRequestEvent(date[0], date[1], date[2], date[3]);
		}
	}
	
	private void initialOffice(String office) {
		if (null != office && TimeZone.UK.toString().equals(office)) {
			this.nationIcon.setResource(MeetingResources.I.flagUK());
		}else{
			this.nationIcon.setResource(MeetingResources.I.flagBlank());
		}
	}
	
	@SuppressWarnings("deprecation")
	private void initialDate(int offset) {
		this.date = UiTools.newDate(offset);
		this.dateLabel.setText(this.date.getDate() + " " + CalendarMap.getCalendarMonth(new Date(this.date.getTime())));
		this.content.initialClock();
	}
	
	private void initialMeetingEventEditor() {
		if (null == this.meetingEventEditor) {
			this.meetingEventEditor = new MeetingEventEditor();
			this.meetingEventEditor.setHandler(this);
		}
	}
	
	private Room rooms[] = null;
	private int roomPage = 0;
	private void initialRoom(Room[] rooms) {
		if (null != rooms && rooms.length == 4) {
			this.rooms = rooms;
			this.rm1Label.setText(rooms[this.roomPage*4+0].getName());
			this.rm2Label.setText(rooms[this.roomPage*4+1].getName());
			this.rm3Label.setText(rooms[this.roomPage*4+2].getName());
			this.rm4Label.setText(rooms[this.roomPage*4+3].getName());
		}
		this.initialMeetingEventEditor();
		this.meetingEventEditor.initialRoom(rooms);
		this.contentBase.scrollTo(480);
		this.content.initialRoom(rooms);
	}
	
	private void initialStaff(Staff[] staffs) {
		this.initialMeetingEventEditor();
		this.meetingEventEditor.initialStaff(staffs);
	}
	
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		this.fireRequestMeetingSetup();
	}

	private MeetingEventEditor meetingEventEditor = null;
	@Override
	public void onCreateMeeting(int room, int fromY, int toY) {
		int duration[] = this.content.verifyEvent(room, fromY, toY);
		if (null != duration) {
			this.initialMeetingEventEditor();
			this.meetingEventEditor.show(room, fromY, toY, duration, this.date);
//			this.content.addEvent(room, fromY, toY, ""+System.currentTimeMillis(), ""+System.currentTimeMillis());
		}
	}

	@Override
	public void onUpdateMeeting(String id, int room, int from, int to) {
//		this.content.updateEvent(id, room, from, to);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source.equals(this.dateBut)) {
			this.calendarDialog.show(this.date, this.getAbsoluteLeft(), this.getAbsoluteTop());
		}else if (source.equals(this.leftBut)) {
			this.date.setDate(this.date.getDate()-1);
			this.dateLabel.setText(this.date.getDate() + " " + CalendarMap.getCalendarMonth(new Date(this.date.getTime())));
			int isToday = this.isToday();
			this.content.showTimeMask(0 == isToday, isToday >= 0);
			this.requestEventList();
		}else if (source.equals(this.rightBut)) {
			this.date.setDate(this.date.getDate()+1);
			this.dateLabel.setText(this.date.getDate() + " " + CalendarMap.getCalendarMonth(new Date(this.date.getTime())));
			int isToday = this.isToday();
			this.content.showTimeMask(0 == isToday, isToday >= 0);
			this.requestEventList();
		}else if (source.equals(this.todayBut)) {
			this.date = UiTools.newDate();
			this.dateLabel.setText(this.date.getDate() + " " + CalendarMap.getCalendarMonth(new Date(this.date.getTime())));
			int isToday = this.isToday();
			this.content.showTimeMask(0 == isToday, isToday > 0);
			this.requestEventList();
		}
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onDateSelected(CalendarDialog source, int year, int month, int date) {
		this.date.setYear(year-CalendarMap.YEAR_START);
		this.date.setMonth(month-1);
		this.date.setDate(date);
		this.dateLabel.setText(this.date.getDate() + " " + CalendarMap.getCalendarMonth(new Date(this.date.getTime())));
		int isToday = this.isToday();
		this.content.showTimeMask(0 == isToday, isToday > 0);
		this.requestEventList();
	}

	@Override
	public void initial(String office, int offset, Room[] rooms, Staff[] staffs) {
		this.initialOffice(office);
		this.initialDate(offset);
		this.initialRoom(rooms);
		this.initialStaff(staffs);
	}
	
	@Override
	public void onCreate(MeetingEventEditor source) {
		if (null != this.presenter) {
			Meeting meeting = new Meeting();
			meeting.setRoomId(source.getRoomId());
			meeting.setCreatedBy(source.getCreatedBy());
			meeting.setDescription(source.getDescription());
			meeting.setAllDay(0);
			meeting.setStartTime(source.getStartTime());
			meeting.setEndTime(source.getEndTime());
			meeting.setBookingDate(source.getBookingDate());
			meeting.setRepeat(source.getRepeat());
			meeting.setExpireDate(source.getExpriedDate());
			this.presenter.onRequestMeetingBookingEvent(meeting);
		}
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}