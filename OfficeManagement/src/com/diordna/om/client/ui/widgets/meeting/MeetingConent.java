package com.diordna.om.client.ui.widgets.meeting;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.ui.components.VerticalScrollPanel.VerticalScrollContent;
import com.diordna.om.client.ui.util.EventBridge;
import com.diordna.om.client.ui.util.UiTools;
import com.diordna.om.client.ui.widgets.meeting.MeetingEvent.COLOR;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Label;

public class MeetingConent extends Composite implements EventBridge.Feedback, MouseDownHandler, MouseUpHandler, VerticalScrollContent {

	public interface Handler {
		
		public void onCreateMeeting(int room, int from, int to);

		public void onUpdateMeeting(String id, int room, int from, int to);
		
	}
	
	private class Clock extends Timer {

		private boolean started = false;
		
		private Clock() {}
		
		private void startClock(int periodMillis) {
			this.started = true;
			this.scheduleRepeating(periodMillis);
		}
		
		private void stopClock() {
			this.cancel();
			this.started = false;
		}
		
		@Override
		public void run() {
			updateClock(UiTools.newDate());
		}
		
	}
	
	interface MeetingConentUiBinder extends UiBinder<Widget, MeetingConent> {}
	
	private static MeetingConentUiBinder uiBinder = GWT.create(MeetingConentUiBinder.class);
	private static final int UNIT = 225;
	private static final int HEIGHT = 1440;
	@UiField LayoutPanel base;
	@UiField LayoutPanel contentLayout;
	@UiField LayoutPanel overlayLayout;
	@UiField LayoutPanel drawLayout;
	@UiField Label drawText;
	@UiField LayoutPanel timemaskLayout;
	@UiField Label timemaskLabel;
	
	private int room = -1;
	private int y = -1;
	private int mouseY = -1;
	private boolean sectionCheck[][] = new boolean[4][48];
	private Object source = null;
	private Handler handler = null;
	private List<MeetingEvent> eventList = null;
	private Clock clock = new Clock();

	public MeetingConent(Handler handler) {
		this.initWidget(uiBinder.createAndBindUi(this));
		
		for (int i=0; i<this.sectionCheck.length; i++) {
			for (int j=0; j<this.sectionCheck[i].length; j++) {
				this.sectionCheck[i][j] = false;
			}
		}
		
		this.overlayLayout.addDomHandler(this, MouseDownEvent.getType());
		this.overlayLayout.addDomHandler(this, MouseUpEvent.getType());
		
		this.handler = handler;
	}
	
	private boolean isFilled(int room, int from, int to) {
		while (from < to) {
			if (from/30 < this.sectionCheck[room].length) {
				if (this.sectionCheck[room][from/30]) {
					return true;
				}
			}
			from += 30;
		}
		return false;
	}
	
	private boolean isFilled(int room, int index) {
		return index < this.sectionCheck[room].length && this.sectionCheck[room][index];
	}
	
	private void fillSelection(int room, int from, int to, boolean value) {
		while (from < to) {
			if (from/30 < this.sectionCheck[room].length) {
				this.sectionCheck[room][from/30] = value;
			}
			from += 30;
		}
	}
	
	@SuppressWarnings("deprecation")
	private void updateClock(Date date) {
		int h = date.getHours();
		int mi = date.getMinutes();
		this.timemaskLabel.setText((h < 10 ? "0" + h : h) + ":" + (mi < 10 ? "0" + mi : mi) + (h < 13 ? " AM" : " PM"));
		this.base.setWidgetTopHeight(this.timemaskLayout, 0, Unit.PX, date.getHours()*60+date.getMinutes(), Unit.PX);
	}
	
	public void initialClock() {
		this.updateClock(UiTools.newDate());
		this.clock.startClock(10000);
	}
	
	public void showTimeMask(boolean show, boolean editable) {
		if (show) {
			if (!this.clock.started) {
				this.updateClock(UiTools.newDate());
				this.clock.startClock(10000);
			}
		}else{
			if (this.clock.started) {
				this.clock.stopClock();
				this.base.setWidgetTopHeight(this.timemaskLayout, 0, Unit.PX, 0, Unit.PX);
			}
		}
		if (editable) {
			this.base.setWidgetTopBottom(this.overlayLayout, 0, Unit.PX, 0, Unit.PX);
		}else{
			this.base.setWidgetTopHeight(this.overlayLayout, 0, Unit.PX, 0, Unit.PX);
		}
		// TODO Clean the panel..
	}
	
	public void addEvent(int room, int fromY, int toY, String id, String title) {
		if (fromY < toY) {
			int from = fromY / 30 * 30;
			int to = (toY / 30 + (toY % 30>0?1:0)) * 30;
			if (0 != to && !this.isFilled(room, from, to)) {
				if (from == to) {
					to = from + 30; 
				}
				MeetingEvent meetingEvent = new MeetingEvent(id, title);
				if (0 == room) {
					meetingEvent.setColor(COLOR.RED);
				}else if (1 == room) {
					meetingEvent.setColor(COLOR.ORANGE);
				}else if (2 == room) {
					meetingEvent.setColor(COLOR.GREEN);
				}else if (3 == room) {
					meetingEvent.setColor(COLOR.BLUE);
				}else{
					meetingEvent.setColor(COLOR.RED);
				}
				meetingEvent.setEvent(room, from, to);
				this.fillSelection(room, from, to, true);
				this.contentLayout.add(meetingEvent);
				this.contentLayout.setWidgetLeftWidth(meetingEvent, room*25, Unit.PCT, 25, Unit.PCT);
				this.contentLayout.setWidgetTopHeight(meetingEvent, from, Unit.PX, to-from, Unit.PX);
				if (null == this.eventList) {
					this.eventList = new ArrayList<MeetingEvent>();
				}
				this.eventList.add(meetingEvent);
			}
		}else if (fromY > toY) {
			int from = fromY / 30 * 30;
			int to = toY / 30 * 30;
			if (0 != to && !this.isFilled(room, to, from)) {
				if (from == to) {
					to = from + 30; 
				}
				MeetingEvent meetingEvent = new MeetingEvent(id, title);
				if (0 == room) {
					meetingEvent.setColor(COLOR.RED);
				}else if (1 == room) {
					meetingEvent.setColor(COLOR.ORANGE);
				}else if (2 == room) {
					meetingEvent.setColor(COLOR.GREEN);
				}else if (3 == room) {
					meetingEvent.setColor(COLOR.BLUE);
				}else{
					meetingEvent.setColor(COLOR.RED);
				}
				meetingEvent.setEvent(room, to, from);
				this.fillSelection(room, to, from, true);
				this.contentLayout.add(meetingEvent);
				this.contentLayout.setWidgetLeftWidth(meetingEvent, room*25, Unit.PCT, 25, Unit.PCT);
				this.contentLayout.setWidgetTopHeight(meetingEvent, to, Unit.PX, from-to, Unit.PX);
			}
		}
	}
	
	public void updateEvent(String id, int room, int from, int to) {
		if (null != this.selectedEvent && id.equals(this.selectedEvent.getId())) {
			this.selectedEvent.setRoom(room);
			this.selectedEvent.setFrom(from);
			this.selectedEvent.setTo(to);
			this.fillSelection(this.selectedEvent.getRoom(), this.selectedEvent.getFrom(), this.selectedEvent.getTo(), true);
		}
	}
	
	public int[] verifyEvent(int room, int fromY, int toY) {
		int result[] = null;
		if (fromY < toY) {
			int from = fromY / 30 * 30;
			int to = (toY / 30 + (toY % 30>0?1:0)) * 30;
			if (0 != to && !this.isFilled(room, from, to)) {
				if (from == to) {
					to = from + 30; 
				}
				result = new int[]{from, to};
			}
		}else if (fromY > toY) {
			int from = fromY / 30 * 30;
			int to = toY / 30 * 30;
			if (0 != to && !this.isFilled(room, to, from)) {
				if (from == to) {
					to = from + 30; 
				}
				result = new int[]{from, to};
			}
		}
		return result;
	}

	private Room rooms[] = null;
	private int roomPage = 0;
	public void initialRoom(Room[] rooms) {
		this.rooms = rooms;
	}
	
	@Override
	public int getHeight() {
		return HEIGHT;
	}
	
	private MeetingEvent selectedEvent = null;
	private MeetingEvent rollbackEvent = null;
	@Override
	public void onMouseDown(MouseDownEvent event) {
		this.source = event.getSource();
		if (this.source.equals(this.overlayLayout)) {
			this.room = (this.roomPage * 4) + ((event.getClientX() - this.overlayLayout.getAbsoluteLeft()) / UNIT);
			this.y = event.getClientY() - this.overlayLayout.getAbsoluteTop();
			int index = this.y / 30;
			this.selectedEvent = null;
			this.rollbackEvent = null;
			if (null != this.eventList && this.isFilled(this.room, index)) {
				for (int i=0; i<this.eventList.size(); i++) {
					MeetingEvent meetingEvent = this.eventList.get(i);
					if (meetingEvent.isMyEvent(this.room, this.y)) {
						this.selectedEvent = meetingEvent;
						this.rollbackEvent = meetingEvent.copy(i);
						this.fillSelection(meetingEvent.getRoom(), meetingEvent.getFrom(), meetingEvent.getTo(), false);
						break;
					}
				}
			}else {
				this.overlayLayout.setWidgetLeftWidth(this.drawLayout, this.room*25, Unit.PCT, 25, Unit.PCT);
			}
			EventBridge.hookEvent(Event.ONMOUSEMOVE | Event.ONMOUSEUP, this);
		}
	}
	
	@Override
	public void onMouseMoveEvent(Event event) {
		if (null != this.source) {
			if (this.source.equals(this.overlayLayout)) {
				this.mouseY  = event.getClientY() - this.overlayLayout.getAbsoluteTop();
				int moveTo = this.mouseY / 30 * 30;
				int room = (event.getClientX() - this.overlayLayout.getAbsoluteLeft()) / UNIT;
				if (this.y < this.mouseY) {
					if (null == this.selectedEvent) {
						int from = this.y / 30 * 30;
						int to = (this.mouseY / 30 + (this.mouseY % 30>0?1:0)) * 30;
						this.drawLayout.setWidgetBottomHeight(this.drawText, 5, Unit.PX, 18, Unit.PX);
						this.overlayLayout.setWidgetTopHeight(this.drawLayout, from, Unit.PX, to-from, Unit.PX);
						this.drawText.setText((from / 60)+":"+(from % 60 == 0?"00":"30") + " - " + (to / 60)+":"+(to % 60 == 0?"00":"30"));
					}else {
						if (0 == room) {
							this.selectedEvent.setColor(COLOR.RED);
						}else if (1 == room) {
							this.selectedEvent.setColor(COLOR.ORANGE);
						}else if (2 == room) {
							this.selectedEvent.setColor(COLOR.GREEN);
						}else if (3 == room) {
							this.selectedEvent.setColor(COLOR.BLUE);
						}else{
							this.selectedEvent.setColor(COLOR.RED);
						}
						this.contentLayout.setWidgetLeftWidth(this.selectedEvent, room*25, Unit.PCT, 25, Unit.PCT);
						this.contentLayout.setWidgetTopHeight(this.selectedEvent, moveTo, Unit.PX, this.selectedEvent.getTo()-this.selectedEvent.getFrom(), Unit.PX);
					}
				}else if (this.y > this.mouseY) {
					if (null == this.selectedEvent) {
						int from = this.y / 30 * 30;
						int to = this.mouseY / 30 * 30;
						this.drawLayout.setWidgetTopHeight(this.drawText, 5, Unit.PX, 18, Unit.PX);
						this.overlayLayout.setWidgetTopHeight(this.drawLayout, to, Unit.PX, from-to, Unit.PX);
						this.drawText.setText((to / 60)+":"+(to % 60 == 0?"00":"30") + " - " + (from / 60)+":"+(from % 60 == 0?"00":"30"));
					}else {
						if (0 == room) {
							this.selectedEvent.setColor(COLOR.RED);
						}else if (1 == room) {
							this.selectedEvent.setColor(COLOR.ORANGE);
						}else if (2 == room) {
							this.selectedEvent.setColor(COLOR.GREEN);
						}else if (3 == room) {
							this.selectedEvent.setColor(COLOR.BLUE);
						}else{
							this.selectedEvent.setColor(COLOR.RED);
						}
						this.contentLayout.setWidgetLeftWidth(this.selectedEvent, room*25, Unit.PCT, 25, Unit.PCT);
						this.contentLayout.setWidgetTopHeight(this.selectedEvent, moveTo, Unit.PX, this.selectedEvent.getTo()-this.selectedEvent.getFrom(), Unit.PX);
					}
				}
			}
		}
	}

	@Override
	public void onMouseUp(MouseUpEvent event) {
		if (null == this.source) {
			return;
		}
		if (this.source.equals(this.overlayLayout)) {
			this.overlayLayout.setWidgetTopHeight(this.drawLayout, 0, Unit.PX, 0, Unit.PX);
			EventBridge.unhookEvent(Event.ONMOUSEMOVE | Event.ONMOUSEUP, this);
			int y  = event.getClientY() - this.overlayLayout.getAbsoluteTop();
			int moveTo = y / 30 * 30;
			int room = (event.getClientX() - this.overlayLayout.getAbsoluteLeft()) / UNIT;
			if (null == this.selectedEvent) {
				if (null != this.handler) {
					this.handler.onCreateMeeting(this.room, this.y, this.mouseY);
				}
			}else if (this.isFilled(room, moveTo, moveTo+this.selectedEvent.getTo()-this.selectedEvent.getFrom())) {
				this.selectedEvent.setRoom(this.rollbackEvent.getRoom());
				this.selectedEvent.setFrom(this.rollbackEvent.getFrom());
				this.selectedEvent.setTo(this.rollbackEvent.getTo());
				this.selectedEvent.setColor(this.rollbackEvent.getColor());
				this.contentLayout.setWidgetLeftWidth(this.selectedEvent, this.selectedEvent.getRoom()*25, Unit.PCT, 25, Unit.PCT);
				this.contentLayout.setWidgetTopHeight(this.selectedEvent, this.selectedEvent.getFrom(), Unit.PX, this.selectedEvent.getTo()-this.selectedEvent.getFrom(), Unit.PX);
				this.fillSelection(this.rollbackEvent.getRoom(), this.rollbackEvent.getFrom(), this.rollbackEvent.getTo(), true);
			}else{
				if (null != this.handler) {
					int to = moveTo+this.selectedEvent.getTo()-this.selectedEvent.getFrom();
					this.handler.onUpdateMeeting(this.selectedEvent.getId(), room, moveTo, to);
				}
			}
		}
		this.source = null;
		this.y = -1;
		this.mouseY = -1;
		this.room = -1;
	}

	@Override
	public void onMouseUpEvent(Event event) {
		if (null == this.source) {
			return;
		}
		if (this.source.equals(this.overlayLayout)) {
			this.overlayLayout.setWidgetTopHeight(this.drawLayout, 0, Unit.PX, 0, Unit.PX);
			EventBridge.unhookEvent(Event.ONMOUSEMOVE | Event.ONMOUSEUP, this);
			int y  = event.getClientY() - this.overlayLayout.getAbsoluteTop();
			int moveTo = y / 30 * 30;
			int room = (event.getClientX() - this.overlayLayout.getAbsoluteLeft()) / UNIT;
			if (null == this.selectedEvent) {
				if (null != this.handler) {
					this.handler.onCreateMeeting(this.room, this.y, this.mouseY);
				}
			}else if (this.isFilled(room, moveTo, moveTo+this.selectedEvent.getTo()-this.selectedEvent.getFrom())) {
				this.selectedEvent.setRoom(this.rollbackEvent.getRoom());
				this.selectedEvent.setFrom(this.rollbackEvent.getFrom());
				this.selectedEvent.setTo(this.rollbackEvent.getTo());
				this.selectedEvent.setColor(this.rollbackEvent.getColor());
				this.contentLayout.setWidgetLeftWidth(this.selectedEvent, this.selectedEvent.getRoom()*25, Unit.PCT, 25, Unit.PCT);
				this.contentLayout.setWidgetTopHeight(this.selectedEvent, this.selectedEvent.getFrom(), Unit.PX, this.selectedEvent.getTo()-this.selectedEvent.getFrom(), Unit.PX);
				this.fillSelection(this.rollbackEvent.getRoom(), this.rollbackEvent.getFrom(), this.rollbackEvent.getTo(), true);
			}else{
				if (null != this.handler) {
					int to = moveTo+this.selectedEvent.getTo()-this.selectedEvent.getFrom();
					this.handler.onUpdateMeeting(this.selectedEvent.getId(), room, moveTo, to);
				}
			}
		}
		this.source = null;
		this.y = -1;
		this.mouseY = -1;
		this.room = -1;
	}

}