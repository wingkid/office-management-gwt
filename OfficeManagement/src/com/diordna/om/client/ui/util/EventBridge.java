package com.diordna.om.client.ui.util;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;

public class EventBridge {

	public interface Hook {

		public void hookEvent(int type);

		public void unhookEvent(int type);

	}
	
	public interface Feedback {

		public void onMouseMoveEvent(Event event);
		
		public void onMouseUpEvent(Event event);
		
	}

	private static Boolean LOCK = false;
	
	private static List<Feedback> FEEDBACKS = null;

	private static Hook HOOK = null;

	private static void addFeedback(Feedback feedback) {
		if (LOCK) return;
		LOCK = true;
		if (null == FEEDBACKS) {
			FEEDBACKS = new ArrayList<Feedback>();
		}
		FEEDBACKS.add(feedback);
		LOCK = false;
	}
	
	private static void removeFeedback(Feedback feedback) {
		if (LOCK) return;
		LOCK = true;
		if (null != FEEDBACKS) {
			FEEDBACKS.remove(feedback);
		}
		LOCK = false;
	}
	
	private static void fireOnMouseMoveEvent(Event event) {
		if (LOCK) return;
		LOCK = true;
		if (null != FEEDBACKS) {
			for (Feedback feedback : FEEDBACKS) {
				feedback.onMouseMoveEvent(event);
			}
		}
		LOCK = false;
	}
	
	private static void fireOnMouseUpEvent(Event event) {
		if (LOCK) return;
		LOCK = true;
		if (null != FEEDBACKS) {
			for (Feedback feedback : FEEDBACKS) {
				feedback.onMouseUpEvent(event);
			}
		}
		LOCK = false;
	}
	
	public static void setHook(Hook hook) {
		HOOK = hook;
	}

	public static void hookEvent(int type, Feedback feedback) {
		if (null != HOOK) {
			HOOK.hookEvent(type);
		}
		if (null != feedback) {
			addFeedback(feedback);
		}
	}

	public static void unhookEvent(int type, Feedback feedback) {
		if (null != HOOK) {
			HOOK.unhookEvent(type);
		}
		if (null != feedback) {
			removeFeedback(feedback);
		}
	}

	public static void onMouseMoveEvent(Event event) {
		fireOnMouseMoveEvent(event);
	}
	
	public static void onMouseUpEvent(Event event) {
		fireOnMouseUpEvent(event);
	}
	
	public static native void disableTextSelectInternal(Element e, boolean disable)/*-{
		if (disable) {
			e.ondrag = function() {
				return false;
			};
			e.onselectstart = function() {
				return false;
			};
			e.style.MozUserSelect = "none"
		} else {
			e.ondrag = null;
			e.onselectstart = null;
			e.style.MozUserSelect = "text"
		}
	}-*/;

}