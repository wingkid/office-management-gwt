package com.diordna.om.client.ui.util;

import java.util.Date;

public class UiTools {
	
	private static int getFromTime(int value) {
		return value / 30 * 30;
	}
	
	private static int getToTime(int value) {
		return (value / 30 + (value % 30>0?1:0)) * 30;
	}
	
	@SuppressWarnings("deprecation")
	private static int OFFSET = new Date().getTimezoneOffset();
	
	@SuppressWarnings("deprecation")
	public static Date newDate(int offset) {
		if (OFFSET != offset) {
			OFFSET = offset;
		}
		Date date = new Date();
		date.setHours(date.getHours()+((date.getTimezoneOffset() - OFFSET)/60));
		return date;
	}
	
	@SuppressWarnings("deprecation")
	public static Date newDate() {
		Date date = new Date();
		date.setHours(date.getHours()+((date.getTimezoneOffset() - OFFSET)/60));
		return date;
	}
	
	public static String getBetweenTime(int from, int to) {
		from = getFromTime(from);
		to = getToTime(to);
		return (from / 60)+":"+(from % 60 == 0?"00":"30") + " - " + (to / 60)+":"+(to % 60 == 0?"00":"30");
	}
	
}