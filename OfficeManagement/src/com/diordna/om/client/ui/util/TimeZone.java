package com.diordna.om.client.ui.util;

public enum TimeZone {
	
	UK("Europe/London"),
	
	;
	
	private String timezone = null;
	
	TimeZone(String timezone) {
		this.timezone = timezone;
	}
	
	public String toString() {
		return this.timezone;
	}
	
}