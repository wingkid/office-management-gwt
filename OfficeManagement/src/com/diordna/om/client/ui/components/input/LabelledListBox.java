package com.diordna.om.client.ui.components.input;

import com.diordna.om.client.ui.resources.InputResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class LabelledListBox extends Composite implements ChangeHandler {
	
	public interface Handler {
		
		public void onChangeHandler(LabelledListBox source, int index);
		
	}

	interface LabelledListBoxUiBinder extends UiBinder<Widget, LabelledListBox> {}
	
	private static LabelledListBoxUiBinder uiBinder = GWT.create(LabelledListBoxUiBinder.class);
	@UiField LayoutPanel base;
	@UiField VerticalPanel labelBase;
	@UiField Label label;
	@UiField VerticalPanel listboxBase;
	@UiField ListBox listbox;
	private HandlerRegistration listboxHandlerRegistration = null;
	private Handler handler = null;

	public LabelledListBox() {
		InputResources.I.inputCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setHandler(Handler handler) {
		if (null != this.listboxHandlerRegistration) {
			this.listboxHandlerRegistration.removeHandler();
		}
		this.handler = handler;
		if (null != this.handler) {
			this.listboxHandlerRegistration = this.listbox.addChangeHandler(this);
		}
	}

	public void setLabelText(String text) {
		this.label.setText(text);
	}
	
	public void setSelectedIndex(int index) {
		this.listbox.setSelectedIndex(index);
	}
	
	public int getSelectedIndex() {
		return this.listbox.getSelectedIndex();
	}
	
	public String getSelectedValue() {
		int index = this.listbox.getSelectedIndex();
		if (index < 0) {
			return null;
		}
		return this.listbox.getValue(index);
	}
	
	public void addItem(String item, String value) {
		this.listbox.addItem(item, value);
	}

	@Override
	public void onChange(ChangeEvent event) {
		Object source = event.getSource();
		if (source.equals(this.listbox)) {
			this.handler.onChangeHandler(this, this.listbox.getSelectedIndex());
		}
	}

}