package com.diordna.om.client.ui.components.calendar;

import java.util.Date;
import com.diordna.om.client.ui.components.calendar.CalendarMap.CalendarResource;
import com.diordna.om.client.ui.resources.CalendarResources;
import com.diordna.om.client.ui.resources.MeetingResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author Rui Wang
 */
public class CalendarWidget extends Composite implements ClickHandler, MouseMoveHandler, MouseWheelHandler, MouseOverHandler, MouseOutHandler, MouseUpHandler {
	
	public interface Handler {
		
		public void onClickDate(int year, int month, int date);
		
	}
	
	interface CalendarWidgetUiBinder extends UiBinder<Widget, CalendarWidget> {}
	
	private static CalendarWidgetUiBinder uiBinder = GWT.create(CalendarWidgetUiBinder.class);
	@UiField LayoutPanel base;
	@UiField LayoutPanel headerContent;
	@UiField SimplePanel hightlightBlock;
	@UiField Image calImg;
	@UiField Label dateLabel;
	@UiField Button leftBut;
	@UiField Button rightBut;
	@UiField Button dateBut;
	private Date selectedDate = null;
	private CalendarResource calendarResource = null;
	private Handler handler = null;

	public CalendarWidget() {
		MeetingResources.I.meetingCss().ensureInjected();
		CalendarResources.I.calendarCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		this.dateBut.addMouseWheelHandler(this);
		this.leftBut.addClickHandler(this);
		this.rightBut.addClickHandler(this);
		this.calImg.addMouseWheelHandler(this);
		this.calImg.addMouseMoveHandler(this);
		this.calImg.addMouseOverHandler(this);
		this.calImg.addMouseOutHandler(this);
		this.calImg.addMouseUpHandler(this);
	}
	
	private void setCalendar(CalendarResource calendarResource) {
		this.calendarResource = calendarResource;
		this.calImg.setResource(calendarResource.getResource());
	}
	
	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	@SuppressWarnings("deprecation")
	public void setDate(Date date) {
		this.selectedDate = new Date(date.getTime());
		this.selectedDate.setDate(1);
		this.setCalendar(CalendarMap.getCalendarMap(new Date(this.selectedDate.getTime())));
		this.dateLabel.setText(CalendarMap.getCalendarMonth(new Date(this.selectedDate.getTime())));
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onMouseWheel(MouseWheelEvent event) {
		Object source = event.getSource();
		if (source.equals(this.calImg) || source.equals(this.dateBut)) {
			this.selectedDate.setMonth(this.selectedDate.getMonth()+(event.getDeltaY()>0?1:-1));
			this.setCalendar(CalendarMap.getCalendarMap(new Date(this.selectedDate.getTime())));
			this.dateLabel.setText(CalendarMap.getCalendarMonth(new Date(this.selectedDate.getTime())));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source.equals(this.leftBut)) {
			this.selectedDate.setMonth(this.selectedDate.getMonth()-1);
			this.setCalendar(CalendarMap.getCalendarMap(new Date(this.selectedDate.getTime())));
			this.dateLabel.setText(CalendarMap.getCalendarMonth(new Date(this.selectedDate.getTime())));
		}else if (source.equals(this.rightBut)) {
			this.selectedDate.setMonth(this.selectedDate.getMonth()+1);
			this.setCalendar(CalendarMap.getCalendarMap(new Date(this.selectedDate.getTime())));
			this.dateLabel.setText(CalendarMap.getCalendarMonth(new Date(this.selectedDate.getTime())));
		}
	}

	@Override
	public void onMouseMove(MouseMoveEvent event) {
		Object source = event.getSource();
		if (source.equals(this.calImg)) {
			int x = event.getClientX()-this.getAbsoluteLeft();
			int y = event.getClientY()-this.getAbsoluteTop()-70;
			this.headerContent.setWidgetLeftWidth(this.hightlightBlock, x/40*40, Unit.PX, 40, Unit.PX);
			this.headerContent.setWidgetTopHeight(this.hightlightBlock, y/40*40, Unit.PX, 40, Unit.PX);
		}
	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
		Object source = event.getSource();
		if (source.equals(this.calImg)) {
			this.hightlightBlock.setVisible(false);
		}
	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
		Object source = event.getSource();
		if (source.equals(this.calImg)) {
			this.hightlightBlock.setVisible(true);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onMouseUp(MouseUpEvent event) {
		Object source = event.getSource();
		if (source.equals(this.calImg)) {
			int x = (event.getClientX()-this.getAbsoluteLeft())/40;
			int y = (event.getClientY()-this.getAbsoluteTop()-70)/40;
			int offset = y * 7 + x;
			Date date = new Date(this.calendarResource.getStartDate().getTime());
			date.setDate(date.getDate()+offset);
			if (null != this.handler) {
				this.handler.onClickDate(date.getYear()+1900, date.getMonth()+1, date.getDate());
			}
		}
	}

}