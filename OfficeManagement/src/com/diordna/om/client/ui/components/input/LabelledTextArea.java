package com.diordna.om.client.ui.components.input;

import com.diordna.om.client.ui.resources.InputResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;

public class LabelledTextArea extends Composite {

	interface LabelledTextAreaUiBinder extends UiBinder<Widget, LabelledTextArea> {}
	
	private static LabelledTextAreaUiBinder uiBinder = GWT.create(LabelledTextAreaUiBinder.class);
	@UiField LayoutPanel base;
	@UiField VerticalPanel labelBase;
	@UiField VerticalPanel textareaBase;
	@UiField Label label;
	@UiField TextArea testarea;

	public LabelledTextArea() {
		InputResources.I.inputCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setLabelText(String text) {
		this.label.setText(text);
	}
	
	public void setTextAreaHeightInPX(int height) {
		this.testarea.setHeight(height+"PX");
	}
	
	public void setTextAreaText(String text) {
		this.testarea.setText(text);
	}

	public String getTextAreaText() {
		return this.testarea.getText();
	}

}