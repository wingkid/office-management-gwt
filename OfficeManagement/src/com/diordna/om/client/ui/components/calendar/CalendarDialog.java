package com.diordna.om.client.ui.components.calendar;

import java.util.Date;
import com.diordna.om.client.ui.resources.CalendarResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author Rui Wang
 */
public class CalendarDialog extends Composite implements CalendarWidget.Handler {
	
	public interface Handler {
		
		public void onDateSelected(CalendarDialog source, int year, int month, int date);
		
	}

	interface CalendarDialogUiBinder extends UiBinder<Widget, CalendarDialog> {}
	
	private static CalendarDialogUiBinder uiBinder = GWT.create(CalendarDialogUiBinder.class);
	@UiField SimplePanel base;
	@UiField CalendarWidget calendar;
	private DecoratedPopupPanel popupBase = new DecoratedPopupPanel(true);
	private Handler handler = null;

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public CalendarDialog() {
		CalendarResources.I.calendarCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		
		this.popupBase.setStyleName(null);
		this.popupBase.setAnimationEnabled(true);
		this.popupBase.setWidget(this);
		
		this.calendar.setHandler(this);
	}
	
	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	public void show(Date date, int x, int y) {
		this.calendar.setDate(date);
		if (!this.popupBase.isShowing()) {
			this.popupBase.setSize("290PX", "320PX");
			this.popupBase.setPopupPosition(x, y);
			this.popupBase.show();
		}
	}

	@Override
	public void onClickDate(int year, int month, int date) {
		this.popupBase.hide();
		if (null != this.handler) {
			this.handler.onDateSelected(this, year, month, date);
		}
	}

}