package com.diordna.om.client.ui.components.calendar;

import java.util.Date;
import com.diordna.om.client.ui.resources.CalendarResources;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Rui Wang
 */
public class CalendarMap {
	
	public static class CalendarResource {
		
		private ImageResource resource = null;
		private Date startDate = null;
		
		private CalendarResource(ImageResource source, Date date) {
			this.resource = source;
			this.startDate = date;
		}
		
		public ImageResource getResource() {
			return this.resource;
		}

		public Date getStartDate() {
			return this.startDate;
		}

		public static CalendarResource newInstance(ImageResource source, Date date) {
			return new CalendarResource(source, date);
		}
		
	}

	public static final int YEAR_START = 1900;
	
	private static boolean isLeapYear(Date date) {
		@SuppressWarnings("deprecation")
		int year = YEAR_START + date.getYear();
		return (0 == year % 400)||((0 != year % 100) && (0 == year % 40));
	}
	
	private static String getMonth(int month) {
		switch(month) {
		case 0: return "January";
		case 1: return "Feburary";
		case 2: return "March";
		case 3: return "April";
		case 4: return "May";
		case 5: return "June";
		case 6: return "July";
		case 7: return "August";
		case 8: return "September";
		case 9: return "October";
		case 10: return "November";
		case 11: return "December";
		default: return "End of world";
		}
	}
	
	@SuppressWarnings("deprecation")
	public static CalendarResource getCalendarMap(Date date) {
		if (null == date) {
			return null;
		}
		boolean isLeapYear = isLeapYear(date);
		int month = date.getMonth() + 1;
		int day = date.getDay();
		switch (month) {
		case 1: case 8:
			switch(day) {
				case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date311$31(), date);
				case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date312$31(), date);
				case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date313$31(), date);
				case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date314$31(), date);
				case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date315$31(), date);
				case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date316$31(), date);
				case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date317$31(), date);
			};
		case 5: case 7: case 10: case 12:
			switch(day) {
				case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date311$30(), date);
				case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date312$30(), date);
				case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date313$30(), date);
				case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date314$30(), date);
				case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date315$30(), date);
				case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date316$30(), date);
				case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date317$30(), date);
			};
		case 4: case 6: case 9: case 11:
			switch(day) {
				case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date301$31(), date);
				case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date302$31(), date);
				case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date303$31(), date);
				case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date304$31(), date);
				case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date305$31(), date);
				case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date306$31(), date);
				case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date307$31(), date);
			};
		case 3:
			if (isLeapYear) {
				switch(day) {
					case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date311$29(), date);
					case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date312$29(), date);
					case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date313$29(), date);
					case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date314$29(), date);
					case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date315$29(), date);
					case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date316$29(), date);
					case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date317$29(), date);
				};
			}else{
				switch(day) {
					case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date311$28(), date);
					case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date312$28(), date);
					case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date313$28(), date);
					case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date314$28(), date);
					case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date315$28(), date);
					case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date316$28(), date);
					case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date317$28(), date);
				};
			}
		case 2:
			if (isLeapYear) {
				switch(day) {
					case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date291$31(), date);
					case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date292$31(), date);
					case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date293$31(), date);
					case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date294$31(), date);
					case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date295$31(), date);
					case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date296$31(), date);
					case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date297$31(), date);
				};
			}else{
				switch(day) {
					case 1: date.setDate(date.getDate()-7); return CalendarResource.newInstance(CalendarResources.I.date281$31(), date);
					case 2: date.setDate(date.getDate()-1); return CalendarResource.newInstance(CalendarResources.I.date282$31(), date);
					case 3: date.setDate(date.getDate()-2); return CalendarResource.newInstance(CalendarResources.I.date283$31(), date);
					case 4: date.setDate(date.getDate()-3); return CalendarResource.newInstance(CalendarResources.I.date284$31(), date);
					case 5: date.setDate(date.getDate()-4); return CalendarResource.newInstance(CalendarResources.I.date285$31(), date);
					case 6: date.setDate(date.getDate()-5); return CalendarResource.newInstance(CalendarResources.I.date286$31(), date);
					case 0: date.setDate(date.getDate()-6); return CalendarResource.newInstance(CalendarResources.I.date287$31(), date);
				};
			}
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public static String getCalendarMonth(Date date) {
		return getMonth(date.getMonth()) + " " + (YEAR_START+date.getYear());
	}
	
}