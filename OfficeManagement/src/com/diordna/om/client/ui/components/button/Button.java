package com.diordna.om.client.ui.components.button;

import com.diordna.om.client.ui.resources.ButtonResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;

/**
 * @author Rui Wang
 */
public class Button extends Composite implements ClickHandler {
	
	public interface Handler {
		
		public void onClick(Button source);
		
	}
	
	public enum COLOR {
		
		RED,
		
		ORANGE,
		
		GREEN,
		
		BLUE;
		
	}

	interface ButtonUiBinder extends UiBinder<Widget, Button> {}
	
	private static ButtonUiBinder uiBinder = GWT.create(ButtonUiBinder.class);
	@UiField VerticalPanel shadow;
	@UiField VerticalPanel frame;
	@UiField LayoutPanel base;
	@UiField LayoutPanel shell;
	@UiField VerticalPanel textBase;
	@UiField Label textLabel;
	@UiField com.google.gwt.user.client.ui.Button clickButton;
	private Handler handler = null;

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public Button() {
		ButtonResources.I.buttonCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
//		this.shadow.setVisible(false);
		this.clickButton.addMouseOverHandler(new MouseOverHandler() {
			
			@Override
			public void onMouseOver(MouseOverEvent event) {
				shadow.setVisible(true);
				textLabel.setStyleName(ButtonResources.I.buttonCss().textHover());
			}
		});
		this.clickButton.addMouseOutHandler(new MouseOutHandler() {
			
			@Override
			public void onMouseOut(MouseOutEvent event) {
				shadow.setVisible(false);
				textLabel.setStyleName(ButtonResources.I.buttonCss().text());
			}
		});
		this.clickButton.addClickHandler(this);
	}
	
	public void setColor(COLOR color) {
		if (null != color) {
			if (COLOR.RED.equals(color)) {
				this.base.setStyleName(ButtonResources.I.buttonCss().red());
			}else if (COLOR.ORANGE.equals(color)) {
				this.base.setStyleName(ButtonResources.I.buttonCss().orange());
			}else if (COLOR.GREEN.equals(color)) {
				this.base.setStyleName(ButtonResources.I.buttonCss().green());
			}else if (COLOR.BLUE.equals(color)) {
				this.base.setStyleName(ButtonResources.I.buttonCss().blue());
			}else{
				this.base.setStyleName(ButtonResources.I.buttonCss().white());
			}
		}else{
			this.base.setStyleName(ButtonResources.I.buttonCss().white());
		}
	}
	
	public void setText(String text) {
		this.textLabel.setText(text);
	}
	
	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (null != this.handler) {
			this.handler.onClick(this);
		}
	}

}