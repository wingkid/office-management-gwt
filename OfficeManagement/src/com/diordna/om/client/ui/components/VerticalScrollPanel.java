package com.diordna.om.client.ui.components;

import com.diordna.om.client.ui.resources.ComponentResources;
import com.diordna.om.client.ui.util.EventBridge;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;

public class VerticalScrollPanel extends Composite implements EventBridge.Feedback, MouseDownHandler, MouseUpHandler, MouseWheelHandler, ResizeHandler {

	public interface VerticalScrollContent extends IsWidget {

		public int getHeight();

	}

	public interface VerticalScrollPanelUiBinder extends UiBinder<Widget, VerticalScrollPanel> {}

	private static VerticalScrollPanelUiBinder uiBinder = GWT.create(VerticalScrollPanelUiBinder.class);
	@UiField
	LayoutPanel content;
	@UiField
	LayoutPanel base;
	@UiField
	SimplePanel scrollBar;

	private static final int PADDING_TOP = 5;
	private int panelFrom = 0;
	private int scrollFrom = 0;
	private int scrollValue = 0;
	private VerticalScrollContent widget = null;

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public VerticalScrollPanel() {
		ComponentResources.I.componentCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		this.scrollBar.setVisible(false);
		this.scrollBar.addDomHandler(this, MouseDownEvent.getType());
		this.scrollBar.addDomHandler(this, MouseUpEvent.getType());
		Window.addResizeHandler(this);
	}

	private void verifyScrollFrom() {
		if (null != this.widget) {
			int scrollMax = this.getOffsetHeight()-PADDING_TOP*2;
			if (this.scrollFrom < 0) {
				this.scrollFrom = 0;
			} else if (this.scrollFrom + this.scrollValue > scrollMax) {
				this.scrollFrom = scrollMax - this.scrollValue;
			}
		}
	}
	
	private void scrollPanel() {
		int scrollMax = this.getOffsetHeight()-PADDING_TOP*2;
		int panelMax = this.widget.getHeight();
		if (0 == this.scrollFrom) {
			this.panelFrom = 0;
		}else{
			this.panelFrom = (-this.scrollFrom) * panelMax / scrollMax;
		}
		this.content.setWidgetTopHeight(this.widget, this.panelFrom, Unit.PX, this.widget.getHeight(), Unit.PX);
	}
	
	private boolean renderPanel() {
		if (null != this.widget) {
			int windowSize = this.getOffsetHeight();
			int panelSize = this.widget.getHeight();
			if (panelSize > windowSize) {
				if (this.panelFrom > 0) {
					this.panelFrom = 0;
				} else if (this.panelFrom + panelSize < windowSize) {
					this.panelFrom = windowSize - panelSize;
				}
				this.content.setWidgetTopHeight(this.widget, this.panelFrom, Unit.PX, this.widget.getHeight(), Unit.PX);
			} else {
				this.content.setWidgetTopHeight(this.widget, this.panelFrom = 0, Unit.PX, this.widget.getHeight(), Unit.PX);
			}
			return true;
		}else{
			return false;
		}
	}

	private void renderBar() {
		int panelValue = this.getOffsetHeight();
		int panelMax = this.widget.getHeight();
		
		this.scrollBar.setVisible(panelValue < panelMax);
		
		if (this.scrollBar.isVisible()) {
			int scrollMax = this.getOffsetHeight()-PADDING_TOP*2;
			if (0 == this.panelFrom) {
				this.scrollFrom = 0;
			}else{
				this.scrollFrom = (-this.panelFrom) * scrollMax / panelMax;
			}
			this.scrollValue = panelValue * scrollMax / panelMax;
			this.base.setWidgetTopHeight(this.scrollBar, PADDING_TOP+this.scrollFrom, Unit.PX, this.scrollValue, Unit.PX);
		}
	}

	private void render() {
		if (this.renderPanel()) {
			this.renderBar();
		}
	}

	public void setConent(VerticalScrollContent widget) {
		this.content.add(this.widget = widget);
		this.content.setWidgetLeftRight(this.widget, 0, Unit.PX, 0, Unit.PX);
		this.content.setWidgetTopHeight(this.widget, this.panelFrom, Unit.PX, this.widget.getHeight(), Unit.PX);
		this.widget.asWidget().addDomHandler(this, MouseWheelEvent.getType());
	}

	public void refresh() {
		this.render();
	}
	
	public void scrollTo(int to) {
		this.panelFrom -= to;
		this.render();
	}

	@Override
	public void onResize(ResizeEvent event) {
		this.render();
	}

	@Override
	public void onMouseWheel(MouseWheelEvent event) {
		this.panelFrom -= event.getDeltaY()*10;
		this.render();
	}
	
	private Object source = null;
	private int y = -1;
	@Override
	public void onMouseDown(MouseDownEvent event) {
		this.source = event.getSource();
		if (this.scrollBar.equals(this.source)) {
			this.y  = event.getClientY();
			EventBridge.hookEvent(Event.ONMOUSEMOVE | Event.ONMOUSEUP, this);
		}
	}
	
	@Override
	public void onMouseMoveEvent(Event event) {
		if (null != this.source && this.scrollBar.equals(this.source)) {
			Integer cy = event.getClientY();
			this.scrollFrom += cy - this.y;
			this.y = cy;
			this.verifyScrollFrom();
			this.base.setWidgetTopHeight(this.scrollBar, PADDING_TOP+this.scrollFrom, Unit.PX, this.scrollValue, Unit.PX);
			this.scrollPanel();
		}
	}

	@Override
	public void onMouseUp(MouseUpEvent event) {
		if (null == this.source) {
			return;
		}
//		if (this.scrollBar.equals(this.source)) {
			EventBridge.unhookEvent(Event.ONMOUSEMOVE | Event.ONMOUSEUP, this);
//		}
		this.source = null;
		this.y = -1;
	}

	

	@Override
	public void onMouseUpEvent(Event event) {
		if (null == this.source) {
			return;
		}
//		if (this.scrollBar.equals(this.source)) {
			EventBridge.unhookEvent(Event.ONMOUSEMOVE | Event.ONMOUSEUP, this);
//		}
		this.source = null;
		this.y = -1;
	}

}