package com.diordna.om.client.ui.components.input;

import java.util.Date;

import com.diordna.om.client.ui.components.calendar.CalendarDialog;
import com.diordna.om.client.ui.resources.InputResources;
import com.diordna.om.client.ui.util.UiTools;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class LabelledDateBox extends Composite implements CalendarDialog.Handler, ClickHandler {

	interface LabelledDateBoxUiBinder extends UiBinder<Widget, LabelledDateBox> {}
	
	private static LabelledDateBoxUiBinder uiBinder = GWT.create(LabelledDateBoxUiBinder.class);
	@UiField LayoutPanel base;
	@UiField VerticalPanel labelBase;
	@UiField Label label;
	@UiField VerticalPanel listboxBase;
	@UiField TextBox dateBox;
	
	private final static int YEAR_START = 1900;
	private final static String DDMMYYY = "dd/MMM/yyyy";
	private Date date = null;
	private DateTimeFormat format = DateTimeFormat.getFormat(DDMMYYY);
	private CalendarDialog calendarDialog = null;

	public LabelledDateBox() {
		InputResources.I.inputCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		this.dateBox.addClickHandler(this);
		
		this.calendarDialog = new CalendarDialog();
		this.calendarDialog.setHandler(this);
	}
	
	public void setLabelText(String text) {
		this.label.setText(text);
	}

	public void setDateBox(Date date) {
		if (null == date) {
			this.date = null;
			this.dateBox.setText("");
		}else{
			this.date = date;
			this.dateBox.setText(this.format.format(this.date));
		}
	}
	
	public Date getDate() {
		return this.date;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (null == this.date) {
			this.date = UiTools.newDate();
		}
		this.calendarDialog.show(this.date, this.getAbsoluteLeft()+200, this.getAbsoluteTop());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onDateSelected(CalendarDialog source, int year, int month, int date) {
		if (null == this.date) {
			this.date = UiTools.newDate();
		}
		this.date.setYear(year-YEAR_START);
		this.date.setMonth(month-1);
		this.date.setDate(date);
		this.dateBox.setText(this.format.format(this.date));
	}

}