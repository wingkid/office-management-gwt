package com.diordna.om.client.ui.components.input;

import com.diordna.om.client.ui.resources.InputResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;

public class LabelledLabel extends Composite {

	interface LabelledLabelUiBinder extends UiBinder<Widget, LabelledLabel> {}
	
	private static LabelledLabelUiBinder uiBinder = GWT.create(LabelledLabelUiBinder.class);
	@UiField LayoutPanel base;
	@UiField VerticalPanel labelBase;
	@UiField Label label;
	@UiField VerticalPanel valueBase;
	@UiField Label value;

	public LabelledLabel() {
		InputResources.I.inputCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setLabelText(String text) {
		this.label.setText(text);
	}
	
	public void setValueText(String text) {
		this.value.setText(text);
	}

}