package com.diordna.om.client.ui.view;

import com.google.gwt.user.client.ui.IsWidget;

public interface View extends IsWidget {
	
	public void dispose();
	
}