package com.diordna.om.client.ui.view;

import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.dataset.Staff;

public interface MeetingView extends View {
	
	public interface Presenter {

		public void onRequestMeetingInitial();
		
		public void onRequestMeetingBookingEvent(Meeting meeting);
		
		public void onRequestEvent(int year, int month, int date, int offset);

	}
	
	public void setPresenter(Presenter presenter);

	public void initial(String office, int offset, Room[] rooms, Staff[] staffs);
	
}