package com.diordna.om.client.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface InputResources extends ClientBundle {
	
	public final static InputResources I = GWT.create(InputResources.class);
	
	public interface InputCss extends CssResource {
		
		String labelledBackground();
		
		String labelledLabelBackground();

		String labelledLabel();

		String listbox();
		
		String textarea();
		
		String labelBackground();
		
		String label();
		
		String datebox();
		
	}
	
	@Source("com/diordna/om/client/ui/resources/csses/input.css")
	InputCss inputCss();

}