package com.diordna.om.client.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface MeetingResources extends ClientBundle {
	
	public final static MeetingResources I = GWT.create(MeetingResources.class);
	
	public interface MeetingCss extends CssResource {
		
		String background();
		
		String headerBackground();
		
//		String headerBorder();
		
		String headerText();
		
		String headerTextRed();
		
		String headerTextOrange();
		
		String headerTextGreen();
		
		String headerTextBlue();
		
		String headerLineRed();
		
		String headerLineOrange();
		
		String headerLineGreen();
		
		String headerLineBlue();
		
		String headerClick();
		
		String allDayFrame();
		
		String allDayBackground();
		
		String eventHeaderText();

		String eventHeaderRed();

		String eventRed();

		String eventHeaderOrange();

		String eventOrange();

		String eventHeaderGreen();

		String eventGreen();

		String eventHeaderBlue();

		String eventBlue();
		
		String eventEditor();
		
		String eventEditorBackground();
		
		String eventEditorHeaderText();
		
		String eventEditorHeaderRed();
		
		String eventEditorHeaderOrange();
		
		String eventEditorHeaderGreen();
		
		String eventEditorHeaderBlue();
		
	}
	
	@Source("com/diordna/om/client/ui/resources/csses/meeting.css")
	MeetingCss meetingCss();

	@Source("com/diordna/om/client/ui/resources/imgs/background.png")
	@ImageOptions(repeatStyle = RepeatStyle.Horizontal)
	ImageResource background();
	
	@Source("com/diordna/om/client/ui/resources/imgs/australia.png")
	ImageResource flagAustralia();
	
	@Source("com/diordna/om/client/ui/resources/imgs/hungary.png")
	ImageResource flagHungary();
	
	@Source("com/diordna/om/client/ui/resources/imgs/malaysia.png")
	ImageResource flagMalaysia();
	
	@Source("com/diordna/om/client/ui/resources/imgs/united_arab_emirates.png")
	ImageResource flagUAE();
	
	@Source("com/diordna/om/client/ui/resources/imgs/united_kingdom.png")
	ImageResource flagUK();
	
	@Source("com/diordna/om/client/ui/resources/imgs/usa.png")
	ImageResource flagUSA();
	
	@Source("com/diordna/om/client/ui/resources/imgs/blank.png")
	ImageResource flagBlank();
	
}