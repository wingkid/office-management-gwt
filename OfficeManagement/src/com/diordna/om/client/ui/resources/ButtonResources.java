package com.diordna.om.client.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface ButtonResources extends ClientBundle {
	
	public final static ButtonResources I = GWT.create(ButtonResources.class);
	
	public interface ButtonCss extends CssResource {
		
		String whiteButton();
		
		String redButton();
		
		String orangeButton();
		
		String greenButton();
		
		String blueButton();
		
		String white();
		
		String red();
		
		String orange();
		
		String green();
		
		String blue();
		
		String text();
		
		String textHover();
		
	}
	
	@Source("com/diordna/om/client/ui/resources/csses/button.css")
	ButtonCss buttonCss();

}