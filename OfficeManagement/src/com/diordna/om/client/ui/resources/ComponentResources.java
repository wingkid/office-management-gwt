package com.diordna.om.client.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface ComponentResources extends ClientBundle {
	
	public final static ComponentResources I = GWT.create(ComponentResources.class);
	
	public interface ComponentCss extends CssResource {
		
		String scrollBar();
		
	}
	
	@Source("com/diordna/om/client/ui/resources/csses/component.css")
	ComponentCss componentCss();

}