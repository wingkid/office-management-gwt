package com.diordna.om.client.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface RootResources extends ClientBundle {
	
	public final static RootResources I = GWT.create(RootResources.class);
	
	public interface RootCss extends CssResource {
		
		String headerBackground();
		
		String headerBorder();
		
		String shadowLeft();
		
		String shadowRight();
		
	}
	
	@Source("com/diordna/om/client/ui/resources/csses/root.css")
	RootCss rootCss();

	@Source("com/diordna/om/client/ui/resources/imgs/alariclogo.png")
	ImageResource alariclogo();
	
	@Source("com/diordna/om/client/ui/resources/imgs/shadowLeft.png")
	@ImageOptions(repeatStyle = RepeatStyle.Vertical)
	ImageResource shadowLeft();
	
	@Source("com/diordna/om/client/ui/resources/imgs/shadowRight.png")
	@ImageOptions(repeatStyle = RepeatStyle.Vertical)
	ImageResource shadowRight();

}