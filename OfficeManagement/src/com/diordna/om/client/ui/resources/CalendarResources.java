package com.diordna.om.client.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface CalendarResources extends ClientBundle {
	
	public final static CalendarResources I = GWT.create(CalendarResources.class);
	
	public interface CalendarCss extends CssResource {
		
		String background();
		
		String month();
		
		String dialog();
		
	}
	
	@Source("com/diordna/om/client/ui/resources/csses/calendar.css")
	CalendarCss calendarCss();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar_bg.png")
	ImageResource background();

	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal281.png")
	ImageResource date281$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal282.png")
	ImageResource date282$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal283.png")
	ImageResource date283$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal284.png")
	ImageResource date284$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal285.png")
	ImageResource date285$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal286.png")
	ImageResource date286$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal287.png")
	ImageResource date287$31();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal291.png")
	ImageResource date291$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal292.png")
	ImageResource date292$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal293.png")
	ImageResource date293$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal294.png")
	ImageResource date294$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal295.png")
	ImageResource date295$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal296.png")
	ImageResource date296$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal297.png")
	ImageResource date297$31();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal301.png")
	ImageResource date301$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal302.png")
	ImageResource date302$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal303.png")
	ImageResource date303$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal304.png")
	ImageResource date304$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal305.png")
	ImageResource date305$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal306.png")
	ImageResource date306$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal307.png")
	ImageResource date307$31();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal311-31.png")
	ImageResource date311$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal312-31.png")
	ImageResource date312$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal313-31.png")
	ImageResource date313$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal314-31.png")
	ImageResource date314$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal315-31.png")
	ImageResource date315$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal316-31.png")
	ImageResource date316$31();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal317-31.png")
	ImageResource date317$31();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal311-30.png")
	ImageResource date311$30();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal312-30.png")
	ImageResource date312$30();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal313-30.png")
	ImageResource date313$30();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal314-30.png")
	ImageResource date314$30();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal315-30.png")
	ImageResource date315$30();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal316-30.png")
	ImageResource date316$30();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal317-30.png")
	ImageResource date317$30();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal311-29.png")
	ImageResource date311$29();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal312-29.png")
	ImageResource date312$29();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal313-29.png")
	ImageResource date313$29();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal314-29.png")
	ImageResource date314$29();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal315-29.png")
	ImageResource date315$29();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal316-29.png")
	ImageResource date316$29();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal317-29.png")
	ImageResource date317$29();
	
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal311-28.png")
	ImageResource date311$28();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal312-28.png")
	ImageResource date312$28();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal313-28.png")
	ImageResource date313$28();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal314-28.png")
	ImageResource date314$28();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal315-28.png")
	ImageResource date315$28();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal316-28.png")
	ImageResource date316$28();
	@Source("com/diordna/om/client/ui/resources/imgs/calendar/cal317-28.png")
	ImageResource date317$28();
}