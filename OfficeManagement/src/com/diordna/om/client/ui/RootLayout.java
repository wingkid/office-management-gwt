package com.diordna.om.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.diordna.om.client.ui.resources.RootResources;
import com.diordna.om.client.ui.util.EventBridge;
import com.google.gwt.user.client.ui.SimplePanel;

public class RootLayout extends Composite implements EventBridge.Hook, ResizeHandler {

	interface RootLayoutUiBinder extends UiBinder<Widget, RootLayout> {}
	
	private static RootLayout uiInstance = null;
	private static RootLayoutUiBinder uiBinder = GWT.create(RootLayoutUiBinder.class);
	@UiField LayoutPanel base;
	@UiField LayoutPanel headerLayout;
	@UiField SimplePanel contentLayout;
	@UiField SimplePanel leftSide;
	@UiField SimplePanel rightSide;
	@UiField SimplePanel headerBase;

	public RootLayout() {
		RootResources.I.rootCss().ensureInjected();
		this.initWidget(uiBinder.createAndBindUi(this));
		this.repaint();
		EventBridge.setHook(this);
	}
	
	private void repaint() {
		int width = Window.getClientWidth();
		if (width > 0) {
			if (width > 1010) {
				int dif = (width - 1000) / 2;
				this.base.setWidgetLeftWidth(this.leftSide, dif-5, Unit.PX, 5, Unit.PX);
				this.base.setWidgetRightWidth(this.rightSide, dif-5, Unit.PX, 5, Unit.PX);
				this.base.setWidgetLeftRight(this.contentLayout, dif, Unit.PX, dif, Unit.PX);
			}else{
				this.base.setWidgetLeftWidth(this.leftSide, 0, Unit.PX, 5, Unit.PX);
				this.base.setWidgetRightWidth(this.rightSide, 0, Unit.PX, 5, Unit.PX);
				this.base.setWidgetLeftRight(this.contentLayout, 5, Unit.PX, 5, Unit.PX);
			}
		}
		Window.addResizeHandler(this);
	}
	
	public SimplePanel getHeaderBase() {
		return this.headerBase;
	}
	
	public SimplePanel getContentBase() {
		return this.contentLayout;
	}
	
	public static RootLayout getInstance() {
		if (null == uiInstance) {
			uiInstance = new RootLayout();
		}
		return uiInstance;
	}

	@Override
	public void onBrowserEvent(Event event) {
		int type = DOM.eventGetType(event);
		if (Event.ONMOUSEMOVE == type) {
			EventBridge.onMouseMoveEvent(event);
		} else if (Event.ONMOUSEUP == type) {
			EventBridge.onMouseUpEvent(event);
		} else if (Event.ONKEYPRESS == type) {
			if (event.getCharCode() != 505 && event.getCharCode() != 116) {
				super.onBrowserEvent(event);
			}
		} else {
			super.onBrowserEvent(event);
		}
	}
	
	@Override
	public void onResize(ResizeEvent event) {
		this.repaint();
	}

	@Override
	public void hookEvent(int type) {
		EventBridge.disableTextSelectInternal(this.getElement(), true);
		this.sinkEvents(type);
	}

	@Override
	public void unhookEvent(int type) {
		this.unsinkEvents(type);
		EventBridge.disableTextSelectInternal(this.getElement(), false);
	}

}