package com.diordna.om.client.place;

public enum PlaceToken {
	
	METTING("meeting"),
	
	CONTACT("contact"),;
	
	private String token = "default";
	
	PlaceToken(String token) {
		this.token = token;
	}
	
	@Override
	public String toString() {
		return this.token;
	}
	
}