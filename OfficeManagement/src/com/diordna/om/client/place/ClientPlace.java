package com.diordna.om.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ClientPlace extends Place {
	
	public static class Tokenizer implements PlaceTokenizer<ClientPlace> {

		@Override
		public ClientPlace getPlace(String token) {
			return newInstance(token);
		}

		@Override
		public String getToken(ClientPlace place) {
			if (null != place) {
				return place.token;
			} else {
				return null;
			}
		}

	}

	private String token = null;
	
	private ClientPlace() {}

	public static ClientPlace newInstance(String token) {
		ClientPlace cp = new ClientPlace();
		cp.token = token;
		return cp;
	}

	public static ClientPlace newInstance(PlaceToken token) {
		ClientPlace cp = new ClientPlace();
		cp.token = token.toString();
		return cp;
	}

	@Override
	public String toString() {
		return this.token;
	}
	
}