package com.diordna.om.client.mvp;

import com.diordna.om.client.place.ClientPlace;
import com.diordna.om.client.place.PlaceToken;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceHistoryMapper;

public class PlaceMapper implements PlaceHistoryMapper {

	public static PlaceMapper I = new PlaceMapper();
	
	@Override
	public Place getPlace(String token) {
		if (null != token) {
			return ClientPlace.newInstance(token);
		} else {
			return ClientPlace.newInstance(PlaceToken.METTING);
		}
	}

	@Override
	public String getToken(Place place) {
		if (null != place) {
			return place.toString();
		} else {
			return PlaceToken.METTING.toString();
		}
	}

}