package com.diordna.om.client.mvp;

import com.diordna.om.client.activity.MeetingActivity;
import com.diordna.om.client.place.PlaceToken;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class ContentMapper implements ActivityMapper {

	public static ContentMapper I = new ContentMapper();
	
	private MeetingActivity meetingActivity = null;

	private Activity getMeetingActivity() {
		if (null == this.meetingActivity) {
			this.meetingActivity = MeetingActivity.I;
		}
		return this.meetingActivity;
	}

	@Override
	public Activity getActivity(Place place) {
		if (null != place) {
			String token = place.toString();
			if (PlaceToken.METTING.toString().equals(token)) {
				return this.getMeetingActivity();
			}else if (PlaceToken.CONTACT.toString().equals(token)) {
				
			}
		}
		return null;
	}

}