package com.diordna.om.client;

import com.diordna.om.client.controller.ClientController;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class OfficeManagement implements EntryPoint {
	
	public void onModuleLoad() {
		ClientController.getInstance().display(RootLayoutPanel.get());
	}
	
}