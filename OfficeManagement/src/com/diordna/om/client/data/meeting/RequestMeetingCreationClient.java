package com.diordna.om.client.data.meeting;

import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.data.meeting.RequestMeetingCreation;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

public class RequestMeetingCreationClient extends RequestMeetingCreation {

	private JSONObject getMessageHeaderJson() {
		JSONObject json = new JSONObject();
		json.put(MessageHeader.KEY.MSG_ID.toString(), new JSONNumber(this.header.getId().value()));
		json.put(MessageHeader.KEY.MSG_STATUS.toString(), new JSONNumber(this.header.getStatus().value()));
		json.put(MessageHeader.KEY.MSG_CLIENT.toString(), JSONBoolean.getInstance(this.header.isClient()));
		return json;
	}
	
	private JSONObject getMessageContentJson() {
		if (null != this.meeting) {
			JSONObject json = new JSONObject();
//			json.put(KEY.ID.toString(), new JSONNumber(this.meeting.getId()));
			json.put(KEY.ROOM_ID.toString(), new JSONNumber(this.meeting.getRoomId()));
			json.put(KEY.CREATED_BY.toString(), new JSONNumber(this.meeting.getCreatedBy()));
//			json.put(KEY.CREATE_TS.toString(), new JSONNumber(this.meeting.getCreateTS()));
//			json.put(KEY.UPDATED_BY.toString(), new JSONNumber(this.meeting.getUpdatedBy()));
//			json.put(KEY.UPDATE_TS.toString(), new JSONNumber(this.meeting.getUpdateTS()));
			json.put(KEY.DESCRIPTION.toString(), new JSONString(this.meeting.getDescription()));
			json.put(KEY.ALL_DAY.toString(), new JSONNumber(this.meeting.getAllDay()));
			json.put(KEY.START_TIME.toString(), new JSONNumber(this.meeting.getStartTime()));
			json.put(KEY.END_TIME.toString(), new JSONNumber(this.meeting.getEndTime()));
			json.put(KEY.BOOKING_DATE.toString(), new JSONNumber(this.meeting.getBookingDate()));
			json.put(KEY.REPEAT.toString(), new JSONNumber(this.meeting.getRepeat()));
			json.put(KEY.EXPIRE_DATE.toString(), new JSONNumber(this.meeting.getExpireDate()));
			return json;
		}else{
			return null;
		}
	}

	@Override
	protected String getMessageJson() {
		JSONObject json = new JSONObject();
		json.put(HEADER_KEY, this.getMessageHeaderJson());
		JSONObject content = this.getMessageContentJson();
		if (null != content) {
			json.put(CONTENT_KEY, content);
		}
		return json.toString();
	}

	@Override
	public String toString() {
		return this.getMessageJson();
	}
	
	public static RequestMeetingCreationClient newInstance(Meeting meeting) {
		RequestMeetingCreationClient requestMeetingCreationClient = new RequestMeetingCreationClient();
		requestMeetingCreationClient.setData(meeting);
		return requestMeetingCreationClient;
	}

}