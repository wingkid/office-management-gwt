package com.diordna.om.client.data.meeting;

import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.dataset.Staff;
import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.data.meeting.ResponseMeetingInitial;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;

/**
 * @author Rui
 *
 */
public class ResponseMeetingInitialClient extends ResponseMeetingInitial {
	
	private String office = null;
	private int offset = 0;
	private Room rooms[] = null;
	private Staff staffs[] = null;

	private void setData(String data) {
		JSONObject json = JSONParser.parseStrict(data).isObject();
		JSONObject header = json.get(HEADER_KEY).isObject();
		this.setHeader((int)header.get(MessageHeader.KEY.MSG_ID.toString()).isNumber().doubleValue(),
				(int)header.get(MessageHeader.KEY.MSG_STATUS.toString()).isNumber().doubleValue(),
				header.get(MessageHeader.KEY.MSG_CLIENT.toString()).isBoolean().booleanValue());
		JSONObject content = json.get(CONTENT_KEY).isObject();
		this.office = content.get(KEY.OFFICE.toString()).isString().stringValue();
		this.offset = (int)content.get(KEY.OFFSET.toString()).isNumber().doubleValue();
		JSONArray room = content.get(KEY.ROOM.toString()).isArray();
		this.rooms = new Room[room.size()];
		for (int i=0; i<room.size(); i++) {
			JSONObject jsonRoom = room.get(i).isObject();
			this.rooms[i] = new Room();
			this.rooms[i].setId((int)jsonRoom.get(KEY.ROOM_ID.toString()).isNumber().doubleValue());
			this.rooms[i].setName(jsonRoom.get(KEY.ROOM_NAME.toString()).isString().stringValue());
			this.rooms[i].setAvailable((int)jsonRoom.get(KEY.ROOM_AVAILABLE.toString()).isNumber().doubleValue());
		}
		JSONArray staff = content.get(KEY.STAFF.toString()).isArray();
		this.staffs = new Staff[staff.size()];
		for (int i=0; i<staff.size(); i++) {
			JSONObject jsonStaff = staff.get(i).isObject();
			this.staffs[i] = new Staff();
			this.staffs[i].setId((int)jsonStaff.get(KEY.STAFF_ID.toString()).isNumber().doubleValue());
			this.staffs[i].setName(jsonStaff.get(KEY.STAFF_NAME.toString()).isString().stringValue());
		}
	}
	
	@Override
	protected String getMessageJson() {
		return null;
	}
	
	
	
	public String getOffice() {
		return this.office;
	}

	public int getOffset() {
		return this.offset;
	}

	public Room[] getRooms() {
		return this.rooms;
	}

	public Staff[] getStaffs() {
		return this.staffs;
	}

	public static ResponseMeetingInitialClient newInstance(String json) {
		ResponseMeetingInitialClient responseMeetingInitial = new ResponseMeetingInitialClient();
		responseMeetingInitial.setData(json);
		return responseMeetingInitial;
	}

}
