package com.diordna.om.client.data;

import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.data.meeting.RequestMeetingList;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;

/**
 * @author Rui Wang
 */
public class RequestMeetingListClient extends RequestMeetingList {
	
	private JSONObject getMessageHeaderJson() {
		JSONObject json = new JSONObject();
		json.put(MessageHeader.KEY.MSG_ID.toString(), new JSONNumber(this.header.getId().value()));
		json.put(MessageHeader.KEY.MSG_STATUS.toString(), new JSONNumber(this.header.getStatus().value()));
		json.put(MessageHeader.KEY.MSG_CLIENT.toString(), JSONBoolean.getInstance(this.header.isClient()));
		return json;
	}
	
	private JSONObject getMessageContentJson() {
		if (null != this.date) {
			JSONObject json = new JSONObject();
			json.put(KEY.Y.toString(), new JSONNumber(this.date[0]));
			json.put(KEY.M.toString(), new JSONNumber(this.date[1]));
			json.put(KEY.D.toString(), new JSONNumber(this.date[2]));
			json.put(KEY.O.toString(), new JSONNumber(this.date[3]));
			return json;
		}else{
			return null;
		}
	}
	
	protected String getMessageJson() {
		JSONObject json = new JSONObject();
		json.put(HEADER_KEY, this.getMessageHeaderJson());
		JSONObject content = this.getMessageContentJson();
		if (null != content) {
			json.put(CONTENT_KEY, content);
		}
		return json.toString();
	}

	@Override
	public String toString() {
		return this.getMessageJson();
	}

	public static RequestMeetingListClient newInstance(int date[]) {
		RequestMeetingListClient requestMeetingList = new RequestMeetingListClient();
		requestMeetingList.setData(date);
		return requestMeetingList;
	}
	
}