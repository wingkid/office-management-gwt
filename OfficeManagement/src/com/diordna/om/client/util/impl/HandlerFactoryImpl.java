package com.diordna.om.client.util.impl;

import com.diordna.om.client.util.HandlerFactory;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

public class HandlerFactoryImpl implements HandlerFactory {

	private EventBus viewBus = null;
	private EventBus activityBus = null;
	private PlaceController placeController = null;
	
	@Override
	public EventBus viewEventBus() {
		if (null == this.viewBus) {
			this.viewBus = new SimpleEventBus();
		}
		return this.viewBus;
	}

	@Override
	public EventBus activityEventBus() {
		if (null == this.activityBus) {
			this.activityBus = new SimpleEventBus();
		}
		return this.activityBus;
	}

	@Override
	public PlaceController placeController() {
		if (null == this.placeController) {
			this.placeController = new PlaceController(this.activityEventBus());
		}
		return this.placeController;
	}

}