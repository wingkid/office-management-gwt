package com.diordna.om.client.util;

public class ClientTool {
	
	public static void log(int number) {
		log("logging", "INTEGER#: " + number);
	}

	public static void log(boolean bool) {
		log("logging", "BOOLEAN#: " + bool);
	}
	
	public static void log(long bool) {
		log("logging", "Long#: " + bool);
	}
	
	public static void log(double bool) {
		log("logging", "Double#: " + bool);
	}

	public static void log(String msg) {
		log("logging", msg);
	}
	
	public static void log(Object msg) {
		log("logging", msg.toString());
	}

	public static void log(String logName, String msg) {
		if (null == msg) {
			msg = "NULL";
		}
		java.util.logging.Logger.getLogger(logName).info(msg);
	}
	
}