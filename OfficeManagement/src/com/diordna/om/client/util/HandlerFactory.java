package com.diordna.om.client.util;

import com.diordna.om.client.util.impl.HandlerFactoryImpl;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

public interface HandlerFactory {
	
	public static HandlerFactory I = GWT.create(HandlerFactoryImpl.class);
	
	public EventBus viewEventBus();
	
	public EventBus activityEventBus();
	
	public PlaceController placeController();
	
}