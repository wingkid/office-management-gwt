package com.diordna.om.client.controller;

import com.diordna.om.client.model.meeting.MeetingModel;
import com.diordna.om.client.mvp.ContentMapper;
import com.diordna.om.client.mvp.HeaderMapper;
import com.diordna.om.client.mvp.PlaceMapper;
import com.diordna.om.client.place.ClientPlace;
import com.diordna.om.client.place.PlaceToken;
import com.diordna.om.client.ui.RootLayout;
import com.diordna.om.client.util.HandlerFactory;
import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.RootLayoutPanel;

public class ClientController {
	
	static {
		MeetingModel.I.initial();
	}
	
	private static ClientController I = null;
	
	private ClientController() {
		this.initialActivity();
		this.initialHistory();
	}

	private void initialActivity() {
		ActivityManager headerManager = new ActivityManager(HeaderMapper.I, HandlerFactory.I.activityEventBus());
		headerManager.setDisplay(RootLayout.getInstance().getHeaderBase());
		ActivityManager contentManager = new ActivityManager(ContentMapper.I, HandlerFactory.I.activityEventBus());
		contentManager.setDisplay(RootLayout.getInstance().getContentBase());
	}
	
	private void initialHistory() {
		PlaceHistoryHandler placeHistoryHandler = new PlaceHistoryHandler(PlaceMapper.I);
		placeHistoryHandler.register(HandlerFactory.I.placeController(), HandlerFactory.I.activityEventBus(), ClientPlace.newInstance(PlaceToken.METTING));
		placeHistoryHandler.handleCurrentHistory();
	}

	public void display(RootLayoutPanel root) {
		root.add(RootLayout.getInstance());
	}
	
	public static ClientController getInstance() {
		if (null == I) {
			I = new ClientController();
		}
		return I;
	}
	
}