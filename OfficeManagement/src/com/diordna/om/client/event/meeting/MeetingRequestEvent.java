package com.diordna.om.client.event.meeting;

import com.diordna.om.client.dataset.Meeting;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class MeetingRequestEvent extends GwtEvent<MeetingRequestEvent.Handler> {
	
	public interface Handler extends EventHandler {

		public void onRequestMeetingInitial();

		public void onRequestMeetingBookingEvent(Meeting meeting);
		
	}
	
	public static Type<Handler> TYPE = new Type<Handler>();
	private EVENT_TYPE type = null;
	private Meeting meeting = null;
	
	private MeetingRequestEvent() {}
	
	@Override
	protected void dispatch(Handler handler) {
		if (null == this.type) {
			return;
		}else if (EVENT_TYPE.INITIAL.equals(this.type)) {
			handler.onRequestMeetingInitial();
		}else if (EVENT_TYPE.BOOK_MEETING.equals(this.type)) {
			handler.onRequestMeetingBookingEvent(this.meeting);
		}
	}
	
	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	public MeetingRequestEvent onRequestMeetingBookingEvent(Meeting meeting) {
		this.meeting  = meeting;
		return this;
	}

	public static MeetingRequestEvent newInstance(EVENT_TYPE type) {
		MeetingRequestEvent meetingRequestEvent = new MeetingRequestEvent();
		meetingRequestEvent.type = type;
		return meetingRequestEvent;
	}

}