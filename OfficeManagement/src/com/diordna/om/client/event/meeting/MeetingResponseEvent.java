package com.diordna.om.client.event.meeting;

import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.dataset.Staff;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class MeetingResponseEvent extends GwtEvent<MeetingResponseEvent.Handler> {
	
	public interface Handler extends EventHandler {

		public void onResponseMeetingInitial(String office, int offset, Room[] rooms, Staff[] staffs);
		
	}

	public static Type<Handler> TYPE = new Type<Handler>();
	private EVENT_TYPE type = null;
	private String office = null;
	private int offset = 0;
	private Room[] rooms = null;
	private Staff[] staffs = null;
	
	private MeetingResponseEvent() {}
	
	@Override
	protected void dispatch(Handler handler) {
		if (null == this.type) {
			return;
		}else if (EVENT_TYPE.INITIAL.equals(this.type)) {
			handler.onResponseMeetingInitial(this.office, this.offset, this.rooms, this.staffs);
		}
	}
	
	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	public MeetingResponseEvent setData(String office, int offset, Room[] rooms, Staff[] staffs) {
		this.office = office;
		this.offset = offset;
		this.rooms = rooms;
		this.staffs = staffs;
		return this;
	}

	public static MeetingResponseEvent newInstance(EVENT_TYPE type) {
		MeetingResponseEvent meetingRequestEvent = new MeetingResponseEvent();
		meetingRequestEvent.type = type;
		return meetingRequestEvent;
	}

}