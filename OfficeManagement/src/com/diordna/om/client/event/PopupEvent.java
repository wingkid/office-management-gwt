package com.diordna.om.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;

public class PopupEvent extends GwtEvent<PopupEvent.Handler> {
	
	private static PopupEvent LAZY_EVENT = null;

	public interface Handler extends EventHandler {

		public void showWidget(Widget widget);
		
		public void hideWidget();

	}

	public static Type<Handler> TYPE = new Type<Handler>();
	private Widget widget = null;
	private Boolean show = true;

	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(Handler handler) {
		if (this.show) {
			handler.showWidget(this.widget);
		}else{
			handler.hideWidget();
		}
		
	}

	private PopupEvent() {}

	public static PopupEvent getInstance(Widget widget) {
		if (null == LAZY_EVENT) {
			LAZY_EVENT = new PopupEvent();
			LAZY_EVENT.widget = widget;
			LAZY_EVENT.show = true;
		} else {
			LAZY_EVENT.widget = widget;
			LAZY_EVENT.show = true;
		}
		return LAZY_EVENT;
	}
	
	public static PopupEvent newInstance(Widget widget) {
		LAZY_EVENT = new PopupEvent();
		LAZY_EVENT.widget = widget;
		LAZY_EVENT.show = true;
		return LAZY_EVENT;
	}
	
	public static PopupEvent getInstance() {
		if (null == LAZY_EVENT) {
			LAZY_EVENT = new PopupEvent();
			LAZY_EVENT.show = false;
		} else {
			LAZY_EVENT.show = false;
		}
		return LAZY_EVENT;
	}
	
	public static PopupEvent newInstance() {
		LAZY_EVENT = new PopupEvent();
		LAZY_EVENT.show = false;
		return LAZY_EVENT;
	}
	
}