package com.diordna.om.client.activity;

import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.client.dataset.Room;
import com.diordna.om.client.dataset.Staff;
import com.diordna.om.client.event.meeting.MeetingRequestEvent;
import com.diordna.om.client.event.meeting.MeetingResponseEvent;
import com.diordna.om.client.ui.view.MeetingView;
import com.diordna.om.client.ui.widgets.meeting.MeetingLayout;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class MeetingActivity extends AbstractActivity implements MeetingView.Presenter, MeetingResponseEvent.Handler {
	
	public static MeetingActivity I = new MeetingActivity();
	private MeetingView view = null;
	private EventBus eventBus = null;

	private MeetingView getView() {
		if (null == this.view) {
			this.view = GWT.create(MeetingLayout.class);
			this.view.setPresenter(this);
		}
		return this.view;
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		if (null == this.eventBus) {
			this.eventBus = eventBus;
			this.eventBus.addHandler(MeetingResponseEvent.TYPE, this);
		}
		panel.setWidget(this.getView());
	}

	@Override
	public void onRequestEvent(int year, int month, int date, int offset) {
		if (null != this.eventBus) {
//			MeetingRequestEvent meetingRequestEvent = MeetingRequestEvent.getInstance();
//			meetingRequestEvent.onEventList(MeetingRequestEvent.TYPE.EVENT_LIST, RequestMeetingListClient.newInstance(new int[]{year, month, date, offset}));
//			this.eventBus.fireEvent(meetingRequestEvent);
		}
	}

	@Override
	public void onRequestMeetingInitial() {
		if (null != this.eventBus) {
			this.eventBus.fireEvent(MeetingRequestEvent.newInstance(com.diordna.om.client.event.meeting.EVENT_TYPE.INITIAL));
		}
	}
	
	@Override
	public void onRequestMeetingBookingEvent(Meeting meeting) {
		if (null != this.eventBus) {
			this.eventBus.fireEvent(MeetingRequestEvent.newInstance(com.diordna.om.client.event.meeting.EVENT_TYPE.BOOK_MEETING).onRequestMeetingBookingEvent(meeting));
		}
	}

	@Override
	public void onResponseMeetingInitial(String office, int offset, Room[] rooms, Staff[] staffs) {
		// TODO Auto-generated method stub
		this.getView().initial(office, offset, rooms, staffs);
	}

}