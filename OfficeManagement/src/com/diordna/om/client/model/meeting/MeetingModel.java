package com.diordna.om.client.model.meeting;

import com.diordna.om.client.data.meeting.RequestMeetingCreationClient;
import com.diordna.om.client.data.meeting.ResponseMeetingInitialClient;
import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.client.event.meeting.MeetingRequestEvent;
import com.diordna.om.client.event.meeting.MeetingResponseEvent;
import com.diordna.om.client.model.Model;
import com.diordna.om.client.util.HandlerFactory;
import com.diordna.om.shared.type.HttpString;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;

public class MeetingModel extends Model implements MeetingRequestEvent.Handler {
	
	private enum TARGET {
		
		INITIAL,
		
		POST,
		
		GET,
		
	}
	
	public static MeetingModel I = new MeetingModel();
	
	public void initial() {
		HandlerFactory.I.activityEventBus().addHandler(MeetingRequestEvent.TYPE, this);
		this.url += HttpString.REST_MEETING.toString();
	}

	@Override
	public void onRequestMeetingInitial() {
		RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, URL.encode(this.url+HttpString.SEPARATOR.toString()+HttpString.REST_MEETING_INITIAL.toString()));
		this.getRequest(TARGET.INITIAL.toString(), requestBuilder);
	}
	
	@Override
	public void onRequestMeetingBookingEvent(Meeting meeting) {
		RequestMeetingCreationClient requestMeetingCreationClient = RequestMeetingCreationClient.newInstance(meeting);
		RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, URL.encode(this.url));
		this.postRequest(TARGET.POST.toString(), requestBuilder, requestMeetingCreationClient.toString());
	}

	@Override
	protected void onResponseReceived(String target, Request request, Response response) {
		if (TARGET.INITIAL.toString().equals(target)) {
			String text = response.getText();
			ResponseMeetingInitialClient responseMeetingInitialClient = ResponseMeetingInitialClient.newInstance(text);
			HandlerFactory.I.activityEventBus().fireEvent(MeetingResponseEvent.newInstance(com.diordna.om.client.event.meeting.EVENT_TYPE.INITIAL).setData(responseMeetingInitialClient.getOffice(), responseMeetingInitialClient.getOffset(), responseMeetingInitialClient.getRooms(), responseMeetingInitialClient.getStaffs()));
		}
	}

	@Override
	protected void onResponseError(String target, Request request, Throwable throwable) {
		
	}

}