package com.diordna.om.client.model;

import com.diordna.om.shared.type.HttpString;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;

public abstract class Model {
	
	protected String url = GWT.getModuleBaseURL();
	
	private void sendRequest(RequestBuilder requestBuilder, String message, RequestCallback callback) {
		try {
			requestBuilder.setHeader(HttpString.CONTENT_TYPE.toString(), HttpString.CONTENT_TYPE_JSON.toString());
			requestBuilder.sendRequest(message, callback);
		} catch (RequestException e) {
			Window.alert(e.getMessage());
		}
	}
	
	protected void postRequest(final String target, RequestBuilder requestBuilder, String message) {
		this.sendRequest(requestBuilder, message.toString(), new RequestCallback() {

			@Override
			public void onResponseReceived(Request request, Response response) {
				Model.this.onResponseReceived(target, request, response);
			}

			@Override
			public void onError(Request request, Throwable throwable) {
				Model.this.onResponseError(target, request, throwable);
			}
			
		});
	}
	
	protected void getRequest(final String target, RequestBuilder requestBuilder) {
		requestBuilder.setHeader(HttpString.CONTENT_TYPE.toString(), HttpString.CONTENT_TYPE_JSON.toString());
		requestBuilder.setCallback(new RequestCallback() {

			@Override
			public void onResponseReceived(Request request, Response response) {
				Model.this.onResponseReceived(target, request, response);
			}

			@Override
			public void onError(Request request, Throwable throwable) {
				Model.this.onResponseError(target, request, throwable);
			}
			
		});
		try {
			requestBuilder.send();
		} catch (RequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected abstract void onResponseReceived(String target, Request request, Response response);

	protected abstract void onResponseError(String target, Request request, Throwable throwable);

}