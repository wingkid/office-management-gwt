package com.diordna.om.server.db.sql;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.bind.JAXBContext;
//import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class SqlManager {
	
	private static SqlManager I = null;
	private static final String PATH = System.getProperty("user.home")+System.getProperty("file.separator")+".om"+System.getProperty("file.separator")+"Sql.cfg.xml";
	private Sqls sqls = null;
	private ConcurrentHashMap<SQL_KEY, String> sqlMap = new ConcurrentHashMap<SQL_KEY, String>();

	private SqlManager() {
		this.loadPool();
	}
	
	private boolean loadPool() {
		try {
			File file = new File(PATH);
			JAXBContext jaxbContext = JAXBContext.newInstance(Sqls.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			this.sqls = (Sqls)unmarshaller.unmarshal(file);
			return null != this.sqls;
		}catch(Exception e){
			// TODO
			return false;
		}
	}
	
	public String getSql(SQL_KEY key) {
		if (null == key) {
			return null;
		}
		String sqlResult = this.sqlMap.get(key);
		if (null != sqlResult ){
			return sqlResult;
		}else{
			if (null != this.sqls && null != this.sqls.getSql() && this.sqls.getSql().size() > 0) {
				for (Sql sql : this.sqls.getSql()) {
					if (sql.getName().equals(key.toString())) {
						this.sqlMap.put(key, sql.getStatement());
						return sql.getStatement();
					}
				}
			}
			return null;
		}
	}
	
	public static SqlManager getInstance() {
		if (null == I) {
			I = new SqlManager();
		}
		return I;
	}
	
}