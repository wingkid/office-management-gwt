package com.diordna.om.server.db.sql;

public enum SQL_KEY {
	
	OFFICE_QUERY_ID_TIMEZONE_FROM_IP(1001),
	
	ROOM_QUERY_ALL_FROM_OFFICE_ID(2001),
	
	STAFF_QUERY_ID_NAME_FROM_OFFICE_ID(3001),
	
	;
	
	private final int code;
	
	SQL_KEY(int code) {
		this.code = code;
	}
	
	public int value() {
		return this.code;
	}
	
}