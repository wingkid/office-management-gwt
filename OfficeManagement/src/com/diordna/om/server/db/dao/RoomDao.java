package com.diordna.om.server.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.diordna.om.server.db.sql.SQL_KEY;

public class RoomDao extends Dao {
	
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String AVAILABLE = "available";
	private static final String OFFICE_ID = "office_id";
	
	private int id = -1;
	private String name = null;
	private int available = -1;
	private int office_id = -1;
	
	public RoomDao() {
		
	}
	
	public RoomDao(Connection connection) {
		super(connection);
	}
	
	@Override
	protected void prepareStatement(SQL_KEY key, PreparedStatement prepareStatement) throws SQLException {
		if (SQL_KEY.ROOM_QUERY_ALL_FROM_OFFICE_ID.equals(key)) {
			prepareStatement.setInt(1, this.getOffice_id());
		}
	}

	@Override
	protected void setResultSet(SQL_KEY key, ResultSet resultSet) throws SQLException {
		if (SQL_KEY.ROOM_QUERY_ALL_FROM_OFFICE_ID.equals(key)) {
			RoomDao roomDao = this;
			boolean firstTime = true;
			while (resultSet.next()) {
				if (!firstTime) {
					roomDao.setNext(new RoomDao());
					roomDao = (RoomDao)roomDao.getNext();
				}
				roomDao.setId(resultSet.getInt(ID));
				roomDao.setName(resultSet.getString(NAME));
				roomDao.setAvailable(resultSet.getInt(AVAILABLE));
				roomDao.setOffice_id(resultSet.getInt(OFFICE_ID));
				firstTime = false;
			}
			roomDao = null;
		}
	}

	public void queryRoomFromOffice(int officeId) {
		this.setOffice_id(officeId);
		this.executePreparedStatementQuery(SQL_KEY.ROOM_QUERY_ALL_FROM_OFFICE_ID);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getOffice_id() {
		return office_id;
	}

	public void setOffice_id(int office_id) {
		this.office_id = office_id;
	}

}