package com.diordna.om.server.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.diordna.om.server.db.sql.SQL_KEY;
import com.diordna.om.server.db.sql.SqlManager;

public abstract class Dao {
	
	protected Connection connection = null;
	private Dao next = null;
	
	protected Dao() {
		
	}
	
	protected Dao(Connection connection) {
		this.connection = connection;
	}
	
	protected abstract void prepareStatement(SQL_KEY key, PreparedStatement prepareStatement) throws SQLException;
	
	protected abstract void setResultSet(SQL_KEY key, ResultSet resultSet) throws SQLException;
	
	protected void executePreparedStatementQuery(SQL_KEY key) {
		String query = SqlManager.getInstance().getSql(key);
		if (null != query && null != this.connection) {
			try {
				PreparedStatement prepareStatement = this.connection.prepareStatement(query);
				this.prepareStatement(key, prepareStatement);
				try {
					ResultSet resultSet = prepareStatement.executeQuery();
					try {
						this.setResultSet(key, resultSet);
					}catch(SQLException e) {
						// TODO
					}finally{
						resultSet.close();
						resultSet = null;
					}
				}catch(SQLException e) {
					// TODO
				}finally{
					prepareStatement.close();
					prepareStatement = null;
				}
			}catch(SQLException e) {
				// TODO
			}
		}
	}

	public Dao getNext() {
		return this.next;
	}

	protected void setNext(Dao next) {
		this.next = next;
	}

}