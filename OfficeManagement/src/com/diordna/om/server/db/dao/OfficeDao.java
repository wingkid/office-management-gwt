package com.diordna.om.server.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.diordna.om.server.db.sql.SQL_KEY;

public class OfficeDao extends Dao {
	
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String ADDRESS1 = "address1";
	private static final String ADDRESS2 = "address2";
	private static final String ADDRESS3 = "address3";
	private static final String ADDRESS4 = "address4";
	private static final String ADDRESS5 = "address5";
	private static final String CITY = "city";
	private static final String COUNTY = "county";
	private static final String COUNTRY = "country";
	private static final String POSTCODE = "postcode";
	private static final String EMAIL = "email";
	private static final String TEL = "tel";
	private static final String FAX = "fax";
	private static final String TIMEZONE = "timezone";
	private static final String IP = "ip";
	
	private int id = -1;
	private String name = null;
	private String address1 = null;
	private String address2 = null;
	private String address3 = null;
	private String address4 = null;
	private String address5 = null;
	private String city = null;
	private String county = null;
	private String country = null;
	private String postcode = null;
	private String email = null;
	private String tel = null;
	private String fax = null;
	private String timezone = null;
	private String ip = null;

	public OfficeDao(Connection connection) {
		super(connection);
	}
	
	@Override
	protected void prepareStatement(SQL_KEY key, PreparedStatement prepareStatement) throws SQLException {
		if (SQL_KEY.OFFICE_QUERY_ID_TIMEZONE_FROM_IP.equals(key)) {
			prepareStatement.setString(1, this.getIp());
		}
	}

	@Override
	protected void setResultSet(SQL_KEY key, ResultSet resultSet) throws SQLException {
		if (SQL_KEY.OFFICE_QUERY_ID_TIMEZONE_FROM_IP.equals(key)) {
			if (resultSet.next()) {
				this.setId(resultSet.getInt(ID));
				this.setTimezone(resultSet.getString(TIMEZONE));
			}
		}
	}
	
	public void queryOfficeFromIp(String ip) {
		this.setIp(ip);
		this.executePreparedStatementQuery(SQL_KEY.OFFICE_QUERY_ID_TIMEZONE_FROM_IP);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getAddress5() {
		return address5;
	}

	public void setAddress5(String address5) {
		this.address5 = address5;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}