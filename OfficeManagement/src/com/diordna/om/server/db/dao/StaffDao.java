package com.diordna.om.server.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.diordna.om.server.db.sql.SQL_KEY;

public class StaffDao extends Dao {
	
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String EXT_NO = "ext_no";
	private static final String EMAIL = "email";
	private static final String MOBILE = "mobile";
	private static final String HOME_TEL = "home_tel";
	private static final String SKYPE_ID = "skype_id";
	private static final String MANAGER_ID = "manager_id";
	private static final String OFFICE_ID = "office_id";
	
	private int id = -1;
	private String name = null;
	private String ext_no = null;
	private String email = null;
	private String mobile = null;
	private String home_tel = null;
	private String skype_id = null;
	private int manager_id = -1;
	private int office_id = -1;
	
	public StaffDao() {
	}
	
	public StaffDao(Connection connection) {
		super(connection);
	}

	@Override
	protected void prepareStatement(SQL_KEY key, PreparedStatement prepareStatement) throws SQLException {
		if (SQL_KEY.STAFF_QUERY_ID_NAME_FROM_OFFICE_ID.equals(key)) {
			prepareStatement.setInt(1, this.getOffice_id());
		}
	}

	@Override
	protected void setResultSet(SQL_KEY key, ResultSet resultSet) throws SQLException {
		if (SQL_KEY.STAFF_QUERY_ID_NAME_FROM_OFFICE_ID.equals(key)) {
			StaffDao staffDao = this;
			boolean firstTime = true;
			while (resultSet.next()) {
				if (!firstTime) {
					staffDao.setNext(new StaffDao());
					staffDao = (StaffDao)staffDao.getNext();
				}
				staffDao.setId(resultSet.getInt(ID));
				staffDao.setName(resultSet.getString(NAME));
				firstTime = false;
			}
			staffDao = null;
		}
	}
	
	public void queryStaffFromOffice(int office) {
		this.setOffice_id(office);
		this.executePreparedStatementQuery(SQL_KEY.STAFF_QUERY_ID_NAME_FROM_OFFICE_ID);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExt_no() {
		return ext_no;
	}

	public void setExt_no(String ext_no) {
		this.ext_no = ext_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getHome_tel() {
		return home_tel;
	}

	public void setHome_tel(String home_tel) {
		this.home_tel = home_tel;
	}

	public String getSkype_id() {
		return skype_id;
	}

	public void setSkype_id(String skype_id) {
		this.skype_id = skype_id;
	}

	public int getManager_id() {
		return manager_id;
	}

	public void setManager_id(int manager_id) {
		this.manager_id = manager_id;
	}

	public int getOffice_id() {
		return office_id;
	}

	public void setOffice_id(int office_id) {
		this.office_id = office_id;
	}

}
