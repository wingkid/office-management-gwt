package com.diordna.om.server.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.server.db.sql.SQL_KEY;

public class MeetingDao extends Dao {
	
	private int meetingSize = 0;
	private Meeting meeting = null;

	public MeetingDao() {
		
	}
	
	public MeetingDao(Connection connection) {
		this.connection = connection;
	}
	
	@Override
	protected void prepareStatement(SQL_KEY key, PreparedStatement prepareStatement) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setResultSet(SQL_KEY key, ResultSet resultSet) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void insertMeeting(Meeting meeting) {
		if (null != meeting) {
			this.meeting = meeting;
			this.meetingSize++;
		}
	}
	
}