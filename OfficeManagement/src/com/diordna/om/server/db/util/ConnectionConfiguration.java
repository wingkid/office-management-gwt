package com.diordna.om.server.db.util;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author Rui Wang
 */
public class ConnectionConfiguration {
	
	public enum DATABASE_TYPE {
		
		MYSQL("jdbc:mysql://");
		
		private String type = null;
		
		DATABASE_TYPE(String type) {
			this.type = type;
		}
		
		@Override
		public String toString() {
			return this.type;
		}
		
	}

	public static ConnectionConfiguration I = new ConnectionConfiguration();
	private static final String PATH = System.getProperty("user.home")+System.getProperty("file.separator")+".om"+System.getProperty("file.separator")+"Connection.cfg.xml";
	private ConnectionPools connectionPools = null;
	
	private ConnectionConfiguration() {}
	
	public boolean loadPool() {
		try {
			File file = new File(PATH);
			JAXBContext jaxbContext = JAXBContext.newInstance(ConnectionPools.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			this.connectionPools = (ConnectionPools)unmarshaller.unmarshal(file);
			return null != this.connectionPools;
		}catch(Exception e){
			// TODO
			return false;
		}
	}
	
	public void savePool() {
		if (null != this.connectionPools) {
			try{
				File file = new File(PATH);
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}
				JAXBContext jaxbContext = JAXBContext.newInstance(ConnectionPools.class);
				Marshaller marshaller = jaxbContext.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				marshaller.marshal(this.connectionPools, file);
			}catch(Exception e) {
				// TODO
				e.printStackTrace();
			}  
		}
	}
	
	public ConnectionPools getConnectionPools() {
		return this.connectionPools;
	}

}