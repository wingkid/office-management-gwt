package com.diordna.om.server.db.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Rui Wang
 */
public class SimpleConnectionManager implements ConnectionManager {

	private class PoolConnection {
		
		private Connection connection = null;
		private boolean inUse = false;
		private int count = 0;
		
		private PoolConnection(Connection connection) {
			this.connection = connection;
		}
		
	}
	
	private static SimpleConnectionManager CONNECTIONPOOL = null;
	private AtomicInteger connectionSize = new AtomicInteger(0);
	private String connectionUrl = null;
	private String testSQL = "SELECT COUNT(*) FROM ";
	private ConnectionPool connectionPool = null;
	private Vector<PoolConnection> connections = null;
	
	private SimpleConnectionManager() {}
	
	private boolean setupConnectionManager() {
		if (!ConnectionConfiguration.I.loadPool()) {
			return false;
		}
		ConnectionPools connectionPools = ConnectionConfiguration.I.getConnectionPools();
		if (connectionPools.getConnectionPool().size() > 0) {
			this.connectionPool = connectionPools.getConnectionPool().get(0);
			this.connectionUrl = this.connectionPool.getType()+this.connectionPool.getHost()+":"+this.connectionPool.getPort()+"/"+this.connectionPool.getSchema();
			this.testSQL += this.connectionPool.getTestTable();
		}
		return true;
	}
	
	private void creatConnections(int connectionNum) throws SQLException {
		for (int i=0; i<connectionNum; i++) {
			if (this.connectionPool.maximum > 0 && this.connectionSize.intValue() >= this.connectionPool.maximum) {
				break;
			}
			try {
				this.connections.add(new PoolConnection(this.newConnection()));
				this.connectionSize.getAndIncrement();
			}catch(SQLException e) {
				throw new SQLException();
			}
		}
	}

	private Connection newConnection() throws SQLException {		
		Connection connection = DriverManager.getConnection(this.connectionUrl, this.connectionPool.getUser(), this.connectionPool.getPassword());
		if (null != this.connections && 0 == this.connectionSize.intValue()) {
			DatabaseMetaData databaseMetaData = connection.getMetaData();
			int maxConnections = databaseMetaData.getMaxConnections();
			if (maxConnections > 0 && this.connectionPool.maximum > maxConnections) {
				this.connectionPool.maximum = maxConnections;
			}
		}
		connection.setAutoCommit(true);
		return connection;
	}
	
	private void closeConnection(Connection connection) {
		try {connection.close();}
		catch(Exception e){}
	}
	
	private void sleep(int millis) {
		try {Thread.sleep(millis);}
		catch(InterruptedException e) {}
	}
	
	private Connection getFreeConnection() throws SQLException {
		Connection connection = this.findFreeConnection();
		if (null == connection) {
			this.creatConnections(this.connectionPool.incremetal);
			connection = this.findFreeConnection();
			if (null == connection) {
				return null;
			}
		}
		return connection;
	}
	
	private Connection findFreeConnection() {
		Connection connection = null;
		PoolConnection poolConnection = null;
		Enumeration<PoolConnection> enumeration = this.connections.elements();
		while (enumeration.hasMoreElements()) {
			poolConnection = enumeration.nextElement();
			if (!poolConnection.inUse) {
				connection = poolConnection.connection;
				poolConnection.inUse = true;
				if (!this.testConnection(connection)) {
					try {
						connection = this.newConnection();
						poolConnection.connection = connection;
					}catch(SQLException e){}
				}
				break;
			}
			
		}
		return connection;
	}

	private boolean testConnection(Connection connection) {
		if (null == connection) {
			return false;
		}
		try {
			if (null != this.connectionPool.testTable && !"".equals(this.connectionPool.testTable)) {
				Statement statement = connection.createStatement();
				statement.execute(this.testSQL);
			}else{
				connection.setAutoCommit(true);
			}
		}catch(Exception e) {
			this.closeConnection(connection);
			return false;
		}
		
		return true;
	}

	@Override
	public synchronized void createPool() throws Exception {
		if (null != this.connections || null == this.connectionPool) {
			return;
		}
		Driver driver = (Driver) Class.forName(this.connectionPool.getDriver()).newInstance();
		DriverManager.registerDriver(driver);
		this.connections = new Vector<PoolConnection>();
		this.creatConnections(this.connectionPool.getInitial());
	}

	@Override
	public synchronized void closePool() throws Exception {
		if (null == this.connections || 0 == this.connectionSize.intValue()) {
			return;
		}
		PoolConnection poolConnection = null;
		Enumeration<PoolConnection> enumeration = this.connections.elements();
		while (enumeration.hasMoreElements()) {
			poolConnection = enumeration.nextElement();
			if (poolConnection.inUse) {
				this.sleep(this.connectionPool.waitingTime);
			}
			this.closeConnection(poolConnection.connection);
			this.connections.removeElement(poolConnection);
		}
		this.connections = null;
		this.connectionSize.getAndSet(0);
	}
	
	@Override
	public synchronized void refreshConnections() throws Exception {
		if (null == this.connections || 0 == this.connectionSize.intValue()) {
			return;
		}
		PoolConnection poolConnection = null;
		Enumeration<PoolConnection> enumeration = this.connections.elements();
		while (enumeration.hasMoreElements()) {
			poolConnection = enumeration.nextElement();
			if (poolConnection.inUse) {
				this.sleep(this.connectionPool.waitingTime);
			}
			this.closeConnection(poolConnection.connection);
			poolConnection.connection = this.newConnection();
			poolConnection.inUse = false;
			poolConnection.count = 0;
		}
	}

	@Override
	public synchronized Connection getConnection() throws Exception {
		if (null == this.connections || 0 == this.connectionSize.intValue()) {
			return null;
		}
		Connection connection = this.getFreeConnection();
		while (null == connection) {
			this.sleep(this.connectionPool.waitingTime);
			connection = this.getFreeConnection();
		}
		return connection;
	}

	@Override
	public synchronized void returnConnection(Connection connection) throws Exception {
		if (null == connection) {
			return;
		}
		if (!connection.getAutoCommit()) {
			connection.commit();
			connection.setAutoCommit(true);
		}
		if (null == this.connections || 0 == this.connectionSize.intValue()) {
			return;
		}
		PoolConnection poolConnection = null;
		Enumeration<PoolConnection> enumeration = this.connections.elements();
		while (enumeration.hasMoreElements()) {
			poolConnection = enumeration.nextElement();
			if (connection == poolConnection.connection) {
				poolConnection.inUse = false;
				poolConnection.count ++;
				if (poolConnection.count >= this.connectionPool.connectionWeight) {
					if (this.connectionSize.intValue() - 1 <= this.connectionPool.initial) {
						this.closeConnection(poolConnection.connection);
						poolConnection.connection = this.newConnection();
						poolConnection.inUse = false;
						poolConnection.count = 0;
					}else{
						this.closeConnection(poolConnection.connection);
						this.connections.removeElement(poolConnection);
						this.connectionSize.getAndDecrement();
					}
				}
			}
		}
	}

	public static ConnectionManager getInstance() {
		if (null == CONNECTIONPOOL) {
			CONNECTIONPOOL = new SimpleConnectionManager();
			if (!CONNECTIONPOOL.setupConnectionManager()) {
				CONNECTIONPOOL = null;
			}else{
				boolean rightDb = false;
				try {
					CONNECTIONPOOL.createPool();
					Connection connection = CONNECTIONPOOL.getConnection();
					rightDb = CONNECTIONPOOL.testConnection(connection);
					CONNECTIONPOOL.returnConnection(connection);
				}catch(Exception e) {
					CONNECTIONPOOL = null;
				}
				if (!rightDb) {
					CONNECTIONPOOL = null;
				}
			}
		}
		return CONNECTIONPOOL;
	}

}