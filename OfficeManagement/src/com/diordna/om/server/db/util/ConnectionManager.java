package com.diordna.om.server.db.util;

import java.sql.Connection;

/**
 * @author Rui Wang
 */
public interface ConnectionManager {
	
	public static ConnectionManager I = SimpleConnectionManager.getInstance();
	
	public void createPool() throws Exception;
	
	public void closePool() throws Exception;
	
	public void refreshConnections() throws Exception;
	
	public void returnConnection(Connection connection) throws Exception;
	
	public Connection getConnection() throws Exception;
	
}