/**
 * 
 */
package com.diordna.om.server.data;

import org.json.JSONObject;

import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.data.meeting.RequestMeetingList;

/**
 * @author Rui
 *
 */
public class RequestMeetingListServer extends RequestMeetingList {

	private void setData(JSONObject data) {
		if (null != data) {
			JSONObject header = data.getJSONObject(HEADER_KEY);
			JSONObject content = data.getJSONObject(CONTENT_KEY);
			if (null != header) {
				this.setHeader(header.getInt(MessageHeader.KEY.MSG_ID.toString()), header.getInt(MessageHeader.KEY.MSG_STATUS.toString()), header.getBoolean(MessageHeader.KEY.MSG_CLIENT.toString()));
			}
			if (null != content) {
				this.setData(new int[]{content.getInt(KEY.Y.toString()), content.getInt(KEY.M.toString()), content.getInt(KEY.D.toString()), content.getInt(KEY.O.toString())});
			}
		}
	}
	
	@Override
	protected String getMessageJson() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static RequestMeetingListServer newInstance(JSONObject data) {
		RequestMeetingListServer requestMeetingList = new RequestMeetingListServer();
		requestMeetingList.setData(data);
		return requestMeetingList;
	}

}