package com.diordna.om.server.data.meeting;

import org.json.JSONObject;
import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.data.meeting.RequestMeetingCreation;

public class RequestMeetingCreationServer extends RequestMeetingCreation {
	
	private void setData(JSONObject data) {
		if (null != data) {
			JSONObject header = data.getJSONObject(HEADER_KEY);
			JSONObject content = data.getJSONObject(CONTENT_KEY);
			if (null != header) {
				this.setHeader(header.getInt(MessageHeader.KEY.MSG_ID.toString()), header.getInt(MessageHeader.KEY.MSG_STATUS.toString()), header.getBoolean(MessageHeader.KEY.MSG_CLIENT.toString()));
			}
			if (null != content) {
				Meeting meeting = new Meeting();
				meeting.setRoomId(content.getInt(KEY.ROOM_ID.toString()));
				meeting.setCreatedBy(content.getInt(KEY.CREATED_BY.toString()));
				meeting.setDescription(content.getString(KEY.DESCRIPTION.toString()));
				meeting.setAllDay(content.getInt(KEY.ALL_DAY.toString()));
				meeting.setStartTime(content.getInt(KEY.START_TIME.toString()));
				meeting.setEndTime(content.getInt(KEY.END_TIME.toString()));
				meeting.setBookingDate(content.getInt(KEY.BOOKING_DATE.toString()));
				meeting.setRepeat(content.getInt(KEY.REPEAT.toString()));
				meeting.setExpireDate(content.getInt(KEY.EXPIRE_DATE.toString()));
				this.setData(meeting);
			}
		}
	}

	@Override
	protected String getMessageJson() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static RequestMeetingCreationServer newInstance(JSONObject data) {
		RequestMeetingCreationServer requestMeetingCreationServer = new RequestMeetingCreationServer();
		requestMeetingCreationServer.setData(data);
		return requestMeetingCreationServer;
	}

}