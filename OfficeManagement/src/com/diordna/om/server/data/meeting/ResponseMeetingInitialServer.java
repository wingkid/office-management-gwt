package com.diordna.om.server.data.meeting;

import org.json.JSONArray;
import org.json.JSONObject;
import com.diordna.om.server.db.dao.RoomDao;
import com.diordna.om.server.db.dao.StaffDao;
import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.data.meeting.ResponseMeetingInitial;

public class ResponseMeetingInitialServer extends ResponseMeetingInitial {
	
	private JSONObject json = null;

	private JSONObject getMessageHeaderJson() {
		JSONObject json = new JSONObject();
		json.put(MessageHeader.KEY.MSG_ID.toString(), this.header.getId().value());
		json.put(MessageHeader.KEY.MSG_STATUS.toString(), this.header.getStatus().value());
		json.put(MessageHeader.KEY.MSG_CLIENT.toString(), this.header.isClient());
		return json;
	}
	
	private JSONObject getMessageContentJson(String office, int offset, RoomDao rooms, StaffDao staffs) {
		JSONObject json = new JSONObject();
		json.put(KEY.OFFICE.toString(), office);
		json.put(KEY.OFFSET.toString(), offset);
		if (null != rooms) {
			JSONArray roomsArray = new JSONArray();
			while (null != rooms) {
				JSONObject room = new JSONObject();
				room.put(KEY.ROOM_ID.toString(), rooms.getId());
				room.put(KEY.ROOM_NAME.toString(), rooms.getName());
				room.put(KEY.ROOM_AVAILABLE.toString(), rooms.getAvailable());
				roomsArray.put(room);
				rooms = (RoomDao)rooms.getNext();
			}
			json.put(KEY.ROOM.toString(), roomsArray);
		}
		if (null != staffs) {
			JSONArray staffArray = new JSONArray();
			while (null != staffs) {
				JSONObject staff = new JSONObject();
				staff.put(KEY.STAFF_ID.toString(), staffs.getId());
				staff.put(KEY.STAFF_NAME.toString(), staffs.getName());
				staffArray.put(staff);
				staffs = (StaffDao)staffs.getNext();
			}
			json.put(KEY.STAFF.toString(), staffArray);
		}
		return json;
	}
	
	private void setData(String office, int offset, RoomDao rooms, StaffDao staffs) {
		this.json = new JSONObject();
		this.json.put(HEADER_KEY, this.getMessageHeaderJson());
		JSONObject content = this.getMessageContentJson(office, offset, rooms, staffs);
		if (null != content) {
			this.json.put(CONTENT_KEY, content);
		}
	}
	
	@Override
	protected String getMessageJson() {
		return this.json.toString();
	}
	
	@Override
	public String toString() {
		return this.getMessageJson();
	}
	
	public static ResponseMeetingInitialServer newInstance(String office, int offset, RoomDao rooms, StaffDao staffs) {
		ResponseMeetingInitialServer responseMeetingInitialServer = new ResponseMeetingInitialServer();
		responseMeetingInitialServer.setData(office, offset, rooms, staffs);
		return responseMeetingInitialServer;
	}

}