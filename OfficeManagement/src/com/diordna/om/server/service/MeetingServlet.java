package com.diordna.om.server.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import com.diordna.om.client.dataset.Meeting;
import com.diordna.om.server.data.meeting.RequestMeetingCreationServer;
import com.diordna.om.server.data.meeting.ResponseMeetingInitialServer;
import com.diordna.om.server.db.dao.MeetingDao;
import com.diordna.om.server.db.dao.OfficeDao;
import com.diordna.om.server.db.dao.RoomDao;
import com.diordna.om.server.db.dao.StaffDao;
import com.diordna.om.shared.type.HttpString;
import com.diordna.om.shared.type.MessageID;

/**
 * @author Rui Wang
 */
public class MeetingServlet extends RestServlet {

	private static final long serialVersionUID = 1L;
	private static final String UK_HOST = "192.168.60";
	private static final String KL_HOST = "192.168.70";
	private static final String RM_HOST = "192.168.55";
	
	private MeetingDao insertMeeting(Connection connection, Meeting meeting) {
		MeetingDao meetingDao = new MeetingDao(connection);
		meetingDao.insertMeeting(meeting);
		return meetingDao;
	}
	
	private OfficeDao getOfficeId(Connection connection) {
		OfficeDao officeDao = new OfficeDao(connection);
		if (this.remoteAddr.startsWith(UK_HOST)) {
			officeDao.queryOfficeFromIp(UK_HOST);
		}else if (this.remoteAddr.startsWith(KL_HOST)) {
			officeDao.queryOfficeFromIp(KL_HOST);
		}else if (this.remoteAddr.startsWith(RM_HOST)) {
			officeDao.queryOfficeFromIp(RM_HOST);
		}else{
			officeDao.queryOfficeFromIp(UK_HOST);
		}
		return officeDao;
	}
	
	private RoomDao getRooms(Connection connection, int officeId) {
		RoomDao roomDao = new RoomDao(connection);
		roomDao.queryRoomFromOffice(officeId);
		return roomDao;
	}
	
	private StaffDao getAllStaff(Connection connection, int officeId) {
		StaffDao staffDao = new StaffDao(connection);
		staffDao.queryStaffFromOffice(officeId);
		return staffDao;
	}
	
	private void getMeetingInitial(HttpServletResponse resp) throws IOException {
		if (!this.databaseConnected.get()) {
			// TODO database is not setup
		}else{
			ResponseMeetingInitialServer responseMeetingInitialServer = null;
			Connection connection = this.getConnection();
			if (null != connection) { 
				OfficeDao officeDao = this.getOfficeId(connection);
				if (-1 != officeDao.getId()) {
					RoomDao roomDao = this.getRooms(connection, officeDao.getId());
					StaffDao staffDao = this.getAllStaff(connection, officeDao.getId());
					
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeZone(TimeZone.getTimeZone(officeDao.getTimezone()));
					int offset = -(calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET)) / (60 * 1000);
					
					responseMeetingInitialServer = ResponseMeetingInitialServer.newInstance(officeDao.getTimezone(), offset, roomDao, staffDao);
				}
				this.close(connection);
				if (null != responseMeetingInitialServer) {
					this.writeResponse(resp, HttpString.CONTENT_TYPE_JSON.toString(), this.getBytes(responseMeetingInitialServer.toString()));
				}
			}else{
				// TODO connection error
			}
		}	
	}
	
	private void postMeetingCreation(JSONObject json, HttpServletResponse resp) throws IOException {
		MessageID messageID = this.readJson(json);
		if (!this.databaseConnected.get()) {
			// TODO database is not setup
		}else if (MessageID.UNKNOWN.equals(messageID)) {
			this.writeResponse(resp, HttpString.CONTENT_TYPE_JSON.toString(), "Welcome".getBytes());
		}else if (MessageID.MEETING_CREATION.equals(messageID)) {
			RequestMeetingCreationServer requestMeetingCreationServer = RequestMeetingCreationServer.newInstance(json);
			Connection connection = this.getConnection();
			if (null != connection) {
				this.insertMeeting(connection, requestMeetingCreationServer.getData());
				
				this.close(connection);
			}else{
				// TODO connection error
			}
		}
	}

	// Service: /officemanagement/meeting/initial
	@Override
	protected void responseGetJson(String pathInfo, HttpServletResponse resp) throws UnsupportedEncodingException, IOException {
		if (pathInfo.endsWith(HttpString.REST_MEETING_INITIAL.toString())) {
			this.getMeetingInitial(resp);
		}
	}

	@Override
	protected void responsePostJson(String pathInfo, JSONObject json, HttpServletResponse resp) throws IOException {
		this.postMeetingCreation(json, resp);
	}

	@Override
	protected void responsePostJson(String pathInfo, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void responsePutJson(String pathInfo, JSONObject json, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void responsePutJson(String pathInfo, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void responseDeleteJson(String pathInfo, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		
	}

}