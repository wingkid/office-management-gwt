package com.diordna.om.server.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import com.diordna.om.server.db.util.ConnectionManager;
import com.diordna.om.shared.data.Message;
import com.diordna.om.shared.data.MessageHeader;
import com.diordna.om.shared.type.HttpString;
import com.diordna.om.shared.type.MessageID;

/**
 * @author Rui Wang
 */
public abstract class RestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int BUFFER_SIZE = 2048;
	private static final Object SYNC_LOCK = new Object();
	protected AtomicBoolean databaseConnected = new AtomicBoolean(false);
	protected String remoteAddr = null;
	
	protected Connection getConnection() {
		Connection connection = null;
		try {
			connection = ConnectionManager.I.getConnection();
		} catch (Exception e) {
			// TODO log
			e.printStackTrace();
			connection = null;
		}
		return connection;
	}
	
	protected void close(Connection connection) {
		if (null != connection) {
			try {
				ConnectionManager.I.returnConnection(connection);
			}catch(Exception e) {
				// TODO
			}finally{
				connection = null;
			}
		}
	}
	
	protected byte[] getBytes(String data) {
		byte bytes[] = null;
		try {
			bytes = data.getBytes(HttpString.ENCODING.toString());
		}catch (UnsupportedEncodingException e) {
			bytes = data.getBytes(); 
		}
		return bytes;
	}
	
	protected void writeResponse(HttpServletResponse response, String contentType, byte data[]) throws IOException {
		response.setContentType(contentType);
		OutputStream os = response.getOutputStream();
		os.write(data);
		os.flush();
		os.close();
	}
	
	protected String readRequest(HttpServletRequest request) throws IOException {
		byte[] bytes = new byte[BUFFER_SIZE];
		StringBuilder inputBuffer = new StringBuilder();
		InputStream is = request.getInputStream();
		int read = 0;
		while ((read = is.read(bytes)) != -1) {
			inputBuffer.append(new String(bytes, 0, read));
		}
		return inputBuffer.toString();
	}
	
	protected JSONObject readJson(String data) {
		try {
			JSONObject json = new JSONObject(data);
			return json;
		}catch(Exception e) {
			return null;
		}
	}
	
	protected MessageID readJson(JSONObject data) {
		JSONObject header = data.getJSONObject(Message.HEADER_KEY);
		if (null != header) {
			return MessageID.valueOf(header.getInt(MessageHeader.KEY.MSG_ID.toString()));
		}
		return MessageID.UNKNOWN;
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		synchronized (SYNC_LOCK) {
			this.databaseConnected.set(null != ConnectionManager.I);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.remoteAddr = req.getRemoteAddr();
		if (HttpString.CONTENT_TYPE_JSON.toString().equals(req.getContentType())) {
			resp.setHeader(HttpString.CONTENT_TYPE.toString(), HttpString.CONTENT_TYPE_JSON.toString());
			this.responseGetJson(req.getPathInfo(), resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.remoteAddr = req.getRemoteAddr();
		if (HttpString.CONTENT_TYPE_JSON.toString().equals(req.getContentType())) {
			resp.setHeader(HttpString.CONTENT_TYPE.toString(), HttpString.CONTENT_TYPE_JSON.toString());
			String rawMessage = this.readRequest(req);
			JSONObject json = this.readJson(rawMessage);
			if (null != json) {
				this.responsePostJson(req.getPathInfo(), json, resp);
			}else{
				this.responsePostJson(req.getPathInfo(), resp);
			}
		}
	}
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.remoteAddr = req.getRemoteAddr();
		if (HttpString.CONTENT_TYPE_JSON.toString().equals(req.getContentType())) {
			resp.setHeader(HttpString.CONTENT_TYPE.toString(), HttpString.CONTENT_TYPE_JSON.toString());
			String rawMessage = this.readRequest(req);
			JSONObject json = this.readJson(rawMessage);
			if (null != json) {
				this.responsePutJson(req.getPathInfo(), json, resp);
			}else{
				this.responsePutJson(req.getPathInfo(), resp);
			}
		}
	}
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.remoteAddr = req.getRemoteAddr();
		if (HttpString.CONTENT_TYPE_JSON.toString().equals(req.getContentType())) {
			resp.setHeader(HttpString.CONTENT_TYPE.toString(), HttpString.CONTENT_TYPE_JSON.toString());
			this.responseDeleteJson(req.getPathInfo(), resp);
		}
	}
	
	protected abstract void responseGetJson(String pathInfo, HttpServletResponse resp) throws IOException;
	
	protected abstract void responsePostJson(String pathInfo, JSONObject json, HttpServletResponse resp) throws IOException;
	
	protected abstract void responsePostJson(String pathInfo, HttpServletResponse resp) throws IOException;
	
	protected abstract void responsePutJson(String pathInfo, JSONObject json, HttpServletResponse resp) throws IOException;
	
	protected abstract void responsePutJson(String pathInfo, HttpServletResponse resp) throws IOException;
	
	protected abstract void responseDeleteJson(String pathInfo, HttpServletResponse resp) throws IOException;
	
}